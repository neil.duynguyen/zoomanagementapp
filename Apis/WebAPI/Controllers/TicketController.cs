﻿using Application.Interfaces;
using Application.Services;
using Application.ViewModels.FoodViewModels;
using Application.ViewModels.TicketViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace WebAPI.Controllers
{
    [ApiController]
    [Route("api/[controller]")]

    public class TicketController : ControllerBase
    {
        private readonly ITicketService _ticketService;

        public TicketController(ITicketService ticketService)
        {
            _ticketService = ticketService;
        }

        [HttpGet]
        public async Task<IActionResult> GetTicket()
        {
            try
            {
                var result = _ticketService.GetTicket();
                return Ok(result);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetTicketById(Guid id)
        {
            try
            {
                var result = await _ticketService.GetTicketById(id);

                return Ok(result);
            }
            catch (Exception ex)
            {
                return NotFound(ex.Message);
            }
        }

        [HttpPost]
        [Authorize(Roles = ("Admin, Manager"))]
        public async Task<IActionResult> CreateTicket(CreateTicketViewModel ticketViewModel)
        {
            try
            {
                var result = await _ticketService.CreateTicket(ticketViewModel);

                return Ok(result);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPut]
        [Authorize(Roles = ("Admin, Manager"))]
        public async Task<IActionResult> UpdateTicket(UpdateTicketModel model)
        {
            try
            {
                var result = await _ticketService.UpdateTicket(model);

                return Ok(result);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }

        }

        [HttpDelete("{id}")]
        [Authorize(Roles = ("Admin, Manager"))]
        public async Task<IActionResult> RemoveTicket(Guid id)
        {
            try
            {
                var result = await _ticketService.RemoveTicket(id);
                return Ok("Xoá thành công.");
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}
