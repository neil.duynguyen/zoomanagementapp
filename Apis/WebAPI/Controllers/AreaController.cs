﻿using Application.Interfaces;
using Application.ViewModels.AreaViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;

namespace WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]

    public class AreaController : Controller
    {
        private readonly IAreaService _areaService;

        public AreaController(IAreaService AreaService)
        {
            _areaService = AreaService;
        }

        [HttpGet]
        public async Task<IActionResult> GetArea()
        {
            try
            {
                var result = await _areaService.GetArea();

                /* if (!result.IsNullOrEmpty())
                     return Ok(result);*/

                return Ok(result);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }

        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetAreaById(Guid id)
        {
            try
            {
                var result = await _areaService.GetAreaById(id);

                return Ok(result);

            }
            catch (Exception ex)
            {
                return NotFound(ex.Message);
            }
        }

        [HttpPost]
        [Authorize(Roles = ("Admin, Manager"))]
        public async Task<IActionResult> CreateArea(CreateAreaViewModels name)
        {
            try
            {
                var result = await _areaService.CreateArea(name);

                return Ok(result);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }


        }

        [HttpPut]
        [Authorize(Roles = ("Admin, Manager"))]
        public async Task<IActionResult> UpdateArea(UpdateAreaViewModel model)
        {
            try
            {
                var result = await _areaService.UpdateArea(model);

                return Ok(result);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }

        }

        [HttpDelete]
        [Authorize(Roles = ("Admin, Manager"))]
        public async Task<IActionResult> RemoveArea(Guid id)
        {
            try
            {
                var result = await _areaService.RemoveArea(id);
                return Ok(result);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}
