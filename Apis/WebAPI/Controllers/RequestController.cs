﻿using Application.Interfaces;
using Application.ViewModels.RequestViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using WebAPI.Validations.RequestViewModelValidations;

namespace WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class RequestController : Controller
    {
        private readonly IRequestService _requestService;

        public RequestController(IRequestService requestService)
        {
            _requestService = requestService;
        }

        [HttpGet]
        public async Task<IActionResult> GetRequest()
        {
            try
            {
                var result = _requestService.GetRequest();

                /* if (!result.IsNullOrEmpty())
                     return Ok(result);*/

                return Ok(result);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }

        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetRequestById(Guid id)
        {
            try
            {
                var result = await _requestService.GetRequestById(id);

                if (result is not null)
                    return Ok(result);

                return NotFound("Không tìm thấy yêu cầu.");
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPost]
        [Authorize(Roles = ("Admin, Staff"))]
        public async Task<IActionResult> CreateRequest(CreateRequestViewModel model)
        {
            try
            {
                var validator = new CreateRequestViewModelValidation();
                var valResult = await validator.ValidateAsync(model);

                if (valResult.Errors.Count != 0)
                {
                    List<string> errors = new List<string>();
                    foreach (var error in valResult.Errors)
                    {
                        var item = error.ErrorMessage; errors.Add(item);
                    }
                    return BadRequest(errors);
                }

                var result = await _requestService.CreateRequest(model);

                return Ok(result);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }


        }

        [HttpPut]
        [Authorize(Roles = ("Admin, Manager"))]
        public async Task<IActionResult> UpdateStatusRequest(UpdateStatusRequestViewModel model)
        {
            try
            {
                var validator = new UpdateRequestViewModelValidation();
                var valResult = await validator.ValidateAsync(model);

                if (valResult.Errors.Count != 0)
                {
                    List<string> errors = new List<string>();
                    foreach (var error in valResult.Errors)
                    {
                        var item = error.ErrorMessage; errors.Add(item);
                    }
                    return BadRequest(errors);
                }

                var result = await _requestService.UpdateStatusRequest(model);

                return Ok(result);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }

        }

        [HttpDelete]
        [Authorize(Roles = ("Admin, Staff"))]
        public async Task<IActionResult> RemoveRequest(Guid id)
        {
            try
            {
                var result = await _requestService.RemoveRequest(id);
                return Ok(result);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }

        }

        [HttpGet("GetRequestByCreateDate")]
        //[Authorize (Roles = "Admin")]
        public async Task<IActionResult> GetRequestByCreateDate(string createDate)
        {
            try
            {
                var result = await _requestService.GetRequestByCreateDate(createDate);

                if (result is not null)
                    return Ok(result);

                return NotFound("Không tìm thấy yêu cầu.");
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}
