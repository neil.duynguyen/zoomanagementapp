﻿using Application.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProfileHistoryController : Controller
    {
        private readonly IProfileHistoryService _profileHistoryService;

        public ProfileHistoryController(IProfileHistoryService profileHistoryService)
        {
            _profileHistoryService = profileHistoryService;
        }

        [HttpGet]
        public async Task<IActionResult> GetProfileHistory()
        {
            try
            {
                var result = await _profileHistoryService.GetProfileHistory();

                /* if (!result.IsNullOrEmpty())
                     return Ok(result);*/

                return Ok(result);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }

        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetProfileHistoryById(Guid id)
        {
            try
            {
                var result = await _profileHistoryService.GetProfileHistoryById(id);

                if (result is not null)
                    return Ok(result);

                return NotFound("Không tìm thấy lịch sử hồ sơ.");
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPost]
        //[Authorize(Roles = ("Admin, Staff"))]
        public async Task<IActionResult> CreateProfileHistory(Guid animalProfileId)
        {
            try
            {
                var result = await _profileHistoryService.CreateProfileHistory(animalProfileId);

                return Ok(result);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }


        }

        [HttpPut("{id}")]
        //[Authorize(Roles = ("Admin, Staff"))]
        public async Task<IActionResult> UpdateProfileHistory(Guid id, Guid animalProfileId)
        {
            try
            {
                var result = await _profileHistoryService.UpdateProfileHistory(id, animalProfileId);

                return Ok(result);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }

        }

        [HttpDelete]
        //[Authorize(Roles = ("Admin, Staff"))]
        public async Task<IActionResult> RemoveProfileHistory(Guid id)
        {
            try
            {
                var result = await _profileHistoryService.RemoveProfileHistory(id);
                return Ok(result);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}
