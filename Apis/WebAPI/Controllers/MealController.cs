﻿using Application.Interfaces;
using Application.Services;
using Application.ViewModels.MealViewModels;
using FluentValidation;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using WebAPI.Validations;
using WebAPI.Validations.MealViewModelValidations;


namespace WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MealController : ControllerBase
    {
        private IMealService _mealService;

        public MealController(IMealService mealService)
        {
            _mealService = mealService;
        }

        [HttpGet("GetMealHistoryByAnimalId/{animalID}")]
        public async Task<IActionResult> GetMealHistoryByAnimalId(Guid animalID)
        {
            try
            {
                return Ok(await _mealService.GetMealByAnimalId(animalID));
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpGet("GetMealByUser")]
        [Authorize(Roles = ("Admin, ZooTrainer, Manager"))]
        public async Task<IActionResult> GetMealByUser()
        {
            try
            {
                return Ok(await _mealService.GetMealByIdUser());
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPost]
        [Authorize(Roles = ("Admin, ZooTrainer, Manager"))]
        public async Task<IActionResult> CreateMeal(CreateMealViewModel createMeal)
        {
            try
            {
                var result = await _mealService.CreateMeal(createMeal);

                return Ok(result);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }

        }

        [HttpPost("StatisticsFood")]
        public async Task<IActionResult> StatisticsFood(StatisticsFoodRequest model)
        {
            
            return Ok(await _mealService.StatisticsFood(model));
        }
    }
}
