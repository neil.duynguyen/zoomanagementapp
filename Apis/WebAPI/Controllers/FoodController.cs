﻿using Application.Interfaces;
using Application.ViewModels.FoodViewModels;
using FluentValidation;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

using WebAPI.Validations.FoodViewModelValidations;

namespace WebAPI.Controllers
{
    [ApiController]
    [Route("api/[controller]")]

    public class FoodController : ControllerBase
    {
        private readonly IFoodService _foodService;

        public FoodController(IFoodService foodService)
        {
            _foodService = foodService;
        }

        [HttpGet]
        public async Task<IActionResult> GetFood()
        {
            try
            {
                var result = _foodService.GetFood();

                return Ok(result);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }

        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetFoodById(Guid id)
        {
            try
            {
                var result = await _foodService.GetFoodById(id);

                return Ok(result);
            }
            catch (Exception ex)
            {
                return NotFound(ex.Message);
            }
        }

        [HttpPost]
        [Authorize(Roles = ("Admin, ZooTrainer, Manager"))]
        public async Task<IActionResult> CreateFood(CreateFoodViewModel foodView)
        {
            var validator = new CreateFoodViewModelValidation();
            var valResult = await validator.ValidateAsync(foodView);

            if (valResult.Errors.Count != 0)
            {
                List<string> errors = new List<string>();
                foreach (var error in valResult.Errors)
                {
                    var item = error.ErrorMessage; errors.Add(item);
                }
                return BadRequest(errors);
            }

            try
            {
                var result = await _foodService.CreateFood(foodView);

                return Ok(result);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }

        }

        [HttpPut]
        [Authorize(Roles = ("Admin, ZooTrainer, Manager"))]
        public async Task<IActionResult> UpdateFood(UpdateFoodViewModel foodView)
        {
            var validator = new UpdateFoodViewModelValidation();
            var valResult = await validator.ValidateAsync(foodView);

            if (valResult.Errors.Count != 0)
            {
                List<string> errors = new List<string>();
                foreach (var error in valResult.Errors)
                {
                    var item = error.ErrorMessage; errors.Add(item);
                }
                return BadRequest(errors);
            }

            try
            {
                var result = await _foodService.UpdateFood(foodView);

                return Ok(result);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpDelete("{id}")]
        [Authorize(Roles = ("Admin, ZooTrainer, Manager"))]
        public async Task<IActionResult> RemoveFood(Guid id)
        {
            try
            {
                var result = await _foodService.RemoveFood(id);
                return Ok(result);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }


    }
}
