﻿using Application.Interfaces;
using Application.ViewModels.OrderViewModels;
using Microsoft.AspNetCore.Mvc;

namespace WebAPI.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class OrderController : ControllerBase
    {
        private readonly IOrderService _orderService;

        public OrderController(IOrderService orderService)
        {
            _orderService = orderService;
        }

        [HttpPost]
        public async Task<IActionResult> CreateOrder(CreateOrderDetailViewModel model)
        {
            try
            {
                var order = await _orderService.CreateOrder(model);

                return Ok(order);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPut]
        public async Task<IActionResult> UpdateOrder(UpdateOrderViewModel updateOrderViewModel)
        {
            try
            {
                await _orderService.UpdateStatusOrder(updateOrderViewModel);
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPost("VNPAY")]
        public async Task<IActionResult> CreatePayment(string idOrder)
        { 
            var paymentUrl = _orderService.CreatePayment(idOrder);
            return Ok(new { PaymentUrl = paymentUrl });
        }
    }
}
