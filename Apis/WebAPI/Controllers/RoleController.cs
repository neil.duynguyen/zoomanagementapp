﻿using Application.Interfaces;
using Application.ViewModels.RoleViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    
    public class RoleController : ControllerBase
    {
        private IRoleService _roleService;
        public RoleController(IRoleService roleService) {
            _roleService = roleService;
        }

        [HttpGet]  
        public async Task<IActionResult> GetRole()
        {
            return Ok(await _roleService.GetRole());
        }

        [HttpPost]
        [Authorize(Roles = ("Admin, Manager"))]
        public async Task<IActionResult> CreateRole(string name)
        {
            try
            {
                var result = await _roleService.CreateRole(name);

                return Ok("Tạo role thành công");
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
            
        }
    }
}
