﻿using Application.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Application.ViewModels.UserViewModels;
using Application.ViewModels.AuthViewModel;
using Microsoft.AspNetCore.Authorization;
using Application.Services;
using Application.ViewModels.FoodViewModels;
using System.Net.Mail;
using System.Net;

namespace WebAPI.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class UserController : ControllerBase
    {
        private readonly IUserService _userService;

        public UserController(IUserService userService)
        {
            _userService = userService;
        }

        [HttpPost]
        [Route("/Register")]
        public async Task<IActionResult> RegisterAsync(RegisterModel loginObject)
        {
            try
            {
                await _userService.RegisterAsync(loginObject);
                return Ok("Register successfully.");
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPost]
        [Route("/RegisterEmployee")]
        [Authorize(Roles = ("Admin, Manager"))]
        public async Task<IActionResult> RegisterEmployeeAsync(RegisterEmployeeModel loginObject)
        {
            try
            {
                await _userService.RegisterEmployeeAsync(loginObject);
                return Ok("Register successfully.");
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPost]
        [Route("/Login")]
        public async Task<IActionResult> LoginAsync(UserLoginViewModel loginObject)
        {
            try
            {
                return Ok(await _userService.LoginAsync(loginObject));
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }

        }

        [HttpGet]
        public async Task<IActionResult> GetUser()
        {
            try
            {
                var result = await _userService.GetUser();

                return Ok(result);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetUser(Guid id)
        {
            try
            {
                var result = await _userService.GetUserById(id);

                return Ok(result);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPut]
        public async Task<IActionResult> UpdateUser(UpdateUserViewModel updateUser)
        {
            try
            {
                var result = await _userService.UpdateUser(updateUser);

                return Ok(result);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }

        }

        [HttpPut("/UpdateRole")]
        [Authorize(Roles = ("Admin"))]
        public async Task<IActionResult> UpdateRoleUser(UpdateUserRoleViewModel updateUserRoleViewModel)
        {
            try
            {
                var result = await _userService.UpdateRoleUser(updateUserRoleViewModel);

                return Ok(result);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }

        }

        [HttpDelete("{id}")]
        [Authorize(Roles = ("Admin, Manager"))]
        public async Task<IActionResult> RemoveUser(Guid id)
        {
            try
            {
                var result = await _userService.RemoveUser(id);

                return Ok(result);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPost("SendEmail")]
        public async Task<IActionResult> SendEmail(SendEmailModel model)
        {
            try
            {
                var result = await _userService.SendEmail(model);
                return Ok(result);

            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

        [HttpPut("ChangePassword")]
        [Authorize(Roles = ("Admin, Manager, Staff, ZooTrainer, User"))]
        public async Task<IActionResult> ChangePassword(ChangePasswordModel model)
        {
            try
            {
                var result = await _userService.ChangePassword(model);
                return Ok(result);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPut("ResetPassword")]
        [Authorize(Roles = ("Admin"))]
        public async Task<IActionResult> ResetPassword(ResetPasswordModel model)
        {
            try
            {
                var result = await _userService.ResetPassword(model);
                return Ok(result);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPut("ForgotPassword")]
        //[Authorize(Roles = ("Admin, Manager, Staff, ZooTrainer"))]
        public async Task<IActionResult> ForgotPassword(ForgotPasswordModel model)
        {
            try
            {
                var result = await _userService.ForgotPassword(model);
                return Ok(result);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}