﻿using Application.Interfaces;
using Application.Services;
using Application.ViewModels.AnimalProfileViewModels;
using Application.ViewModels.ArticleViewModels;
using Application.ViewModels.FoodViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ArticleController : ControllerBase
    {
        private readonly IArticleService _articleService;

        public ArticleController(IArticleService articleService)
        {
            _articleService = articleService;
        }

        [HttpGet]
        public async Task<IActionResult> GetArticle()
        {
            try
            {
                var result = _articleService.GetArticle();

                return Ok(result);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpGet("GetArticleById/{Id}")]
        public async Task<IActionResult> GetArticleById(Guid Id)
        {
            try
            {
                var result = await _articleService.GetArticleById(Id);

                return Ok(result);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpGet("Search")]
        public async Task<IActionResult> SearchArticle([FromQuery] SearchArticleViewModel model)
        {
            try
            {
                var result = await _articleService.SearchArticles(model);

                /* if (!result.IsNullOrEmpty())
                     return Ok(result);*/

                return Ok(result);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }

        }

        [HttpGet("GetArticleByTitle/{title}")]
        public async Task<IActionResult> GetArticleByTitle(string title)
        {
            try
            {
                var result = _articleService.GetArticleByTitle(title);

                return Ok(result);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPost]
        [Authorize(Roles = ("Admin, Staff, Manager"))]
        public async Task<IActionResult> CreateArticle(CreateArticleViewModel foodView)
        {
            try
            {
                var result = await _articleService.CreateArticle(foodView);

                return Ok(result);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPut]
        [Authorize(Roles = ("Admin, Staff, Manager"))]
        public async Task<IActionResult> UpdateArticle(UpdateArticleViewModel foodView)
        {
            try
            {
                var result = await _articleService.UpdateArticle(foodView);

                return Ok(result);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }


        [HttpPut("UpdateStatus")]
        [Authorize(Roles = ("Admin, Staff, Manager"))]
        public async Task<IActionResult> UpdateArticleStatus(UpdateStatusArticleViewModel model)
        {
            try
            {
                var result = await _articleService.UpdateArticleStatus(model);

                return Ok(result);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpDelete("{id}")]
        [Authorize(Roles = ("Admin, Staff, Manager"))]
        public async Task<IActionResult> RemoveArticle(Guid id)
        {
            try
            {
                var result = await _articleService.RemoveArticle(id);
                return Ok(result);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}
