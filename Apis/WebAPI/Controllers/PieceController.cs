﻿using Application.Interfaces;
using Application.ViewModels.PieceViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;

namespace WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    
    public class PieceController : ControllerBase
    {
        private readonly IPieceService _pieceService;

        public PieceController(IPieceService pieceService)
        {
            _pieceService = pieceService;
        }

        [HttpGet]
        public async Task<IActionResult> GetPiece()
        {
            try
            {
                var result = _pieceService.GetPiece();

               /* if (!result.IsNullOrEmpty())
                    return Ok(result);*/

                return Ok(result);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }

        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetPieceById(Guid id)
        {
            try
            {
                var result = await _pieceService.GetPieceById(id);

                if (result is not null)
                    return Ok(result);

                return NotFound("Không tìm thấy loài.");
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPost]
        [Authorize(Roles = ("Admin, Manager"))]
        public async Task<IActionResult> CreatePiece(CreatePieceViewModel createPieceView)
        {
            try
            {
                var result = await _pieceService.CreatePiece(createPieceView);

                return Ok(result);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }


        }

        [HttpPut]
        [Authorize(Roles = ("Admin, Manager"))]
        public async Task<IActionResult> UpdatePiece(UpdatePieceViewModel pieceViewModel)
        {
            try
            {
                var result = await _pieceService.UpdatePiece(pieceViewModel);

                return Ok(result);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }

        }

        [HttpDelete]
        [Authorize(Roles = ("Admin, Manager"))]
        public async Task<IActionResult> RemovePiece(Guid id)
        {
            try
            {
                var result = await _pieceService.RemovePiece(id);
                return Ok(result);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}
