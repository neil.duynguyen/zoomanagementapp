﻿using Application.Interfaces;
using Application.ViewModels.CageViewModels;
using FluentValidation;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using WebAPI.Validations;
using WebAPI.Validations.CageViewModelValidations;

namespace WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]

    public class CageController : Controller
    {
        private readonly ICageService _cageService;
        //private readonly IValidator<CreateCageViewModel> _cageValidator;

        public CageController(ICageService cageService)
        {
            _cageService = cageService;
        }

        [HttpGet]
        public async Task<IActionResult> GetCage()
        {
            try
            {
                var result = _cageService.GetCage();

                /* if (!result.IsNullOrEmpty())
                     return Ok(result);*/

                return Ok(result);
            }
            catch (Exception ex)
            {
                return NotFound(ex.Message);
            }

        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetCageById(Guid id)
        {
            try
            {
                var result = await _cageService.GetCageById(id);
                return Ok(result);
            }
            catch (Exception ex)
            {
                return NotFound(ex.Message);
            }
        }

        [HttpGet("GetCageByAreaId/{id}")]
        public async Task<IActionResult> GetCageByAreaId(Guid id)
        {
            try
            {
                var result = await _cageService.GetCageByAreaID(id);

                return Ok(result);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPost]
        [Authorize(Roles = ("Admin, Manager"))]
        public async Task<IActionResult> CreateCage(CreateCageViewModel model)
        {
            try
            {
                var validator = new CreateCageViewModelValidation();
                var valResult = await validator.ValidateAsync(model);
                //var valResult = _cageValidator.Validate(model);

                if (valResult.Errors.Count != 0)
                {
                    List<string> errors = new List<string>();
                    foreach (var error in valResult.Errors)
                    {
                        var item = error.ErrorMessage; errors.Add(item);
                    }
                    return BadRequest(errors);
                }

                var result = await _cageService.CreateCage(model);

                return Ok(result);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }


        }

        [HttpPut]
        [Authorize(Roles = ("Admin, Manager"))]
        public async Task<IActionResult> UpdateCage(UpdateCageViewModel model)
        {
            try
            {
                var validator = new UpdateCageViewModelValidation();
                var valResult = await validator.ValidateAsync(model);

                if (valResult.Errors.Count != 0)
                {
                    List<string> errors = new List<string>();
                    foreach (var error in valResult.Errors)
                    {
                        var item = error.ErrorMessage; errors.Add(item);
                    }
                    return BadRequest(errors);
                }

                var result = await _cageService.UpdateCage(model);

                return Ok(result);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }

        }

        [HttpDelete]
        [Authorize(Roles = ("Admin, Manager"))]
        public async Task<IActionResult> RemoveCage(Guid id)
        {
            try
            {
                var result = await _cageService.RemoveCage(id);
                return Ok(result);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }

        }
    }
}
