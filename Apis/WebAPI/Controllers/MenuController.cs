﻿using Application.Interfaces;
using Application.Services;
using Application.ViewModels.AnimalProfileViewModels;
using Application.ViewModels.MenuViewModels;
using FluentValidation;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using WebAPI.Validations.MenuViewModelValidations;

namespace WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]

    public class MenuController : ControllerBase
    {
        private IMenuService _menuService;
        private readonly ICurrentTime _currentTime;

        public MenuController(IMenuService menuService, ICurrentTime currentTime)
        {
            _menuService = menuService;
            _currentTime = currentTime;
        }

        [HttpGet]
        public async Task<IActionResult> GetMenu()
        {
            try
            {
                return Ok(await _menuService.GetMenu());
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpGet("GetMenuByUser")]
        [Authorize(Roles = ("Admin, ZooTrainer, Manager"))]
        public async Task<IActionResult> GetMenuByUser()
        {
            try
            {
                return Ok(await _menuService.GetMenuByIdUser());
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpGet("GetMenuByAnimalID/{id}")]
        public async Task<IActionResult> GetMenuByIdAnimal(Guid id)
        {
            try
            {
                return Ok(await _menuService.GetMenuByIdAnimal(id));
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpGet("GetAllMenuByAnimalID/{id}")]
        public async Task<IActionResult> GetAllMenuByIdAnimal(Guid id)
        {
            try
            {
                return Ok(await _menuService.GetAllMenuByIdAnimal(id));
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpGet("Search")]
        public async Task<IActionResult> SearchMenu([FromQuery] SearchMenuModel model)
        {
            try
            {
                var result = await _menuService.SearchMenu(model);

                /* if (!result.IsNullOrEmpty())
                     return Ok(result);*/

                return Ok(result);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }

        }

        [HttpPost]
        //[Authorize(Roles = ("Admin, ZooTrainer, Manager"))]
        public async Task<IActionResult> CreateMenu(CreateMenuViewModel menuView)
        {
            var validator = new CreateMenuViewModelValidation(_currentTime);
            var valResult = await validator.ValidateAsync(menuView);

            if (valResult.Errors.Count != 0)
            {
                List<string> errors = new List<string>();
                foreach (var error in valResult.Errors)
                {
                    var item = error.ErrorMessage; errors.Add(item);
                }
                return BadRequest(errors);
            }

            try
            {
                var result = await _menuService.CreateMenu(menuView);

                return Ok(result);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }

        }

        [HttpPut]
        [Authorize(Roles = ("Admin, ZooTrainer, Manager"))]
        public async Task<IActionResult> UpdateMenu(UpdateMenuViewModel menuView)
        {
            var validator = new UpdateMenuViewModelValidation(_currentTime);
            var valResult = await validator.ValidateAsync(menuView);

            if (valResult.Errors.Count != 0)
            {
                List<string> errors = new List<string>();
                foreach (var error in valResult.Errors)
                {
                    var item = error.ErrorMessage; errors.Add(item);
                }
                return BadRequest(errors);
            }

            try
            {
                var result = await _menuService.UpdateMenu(menuView);

                return Ok(result);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }

        }

        [HttpDelete("{id}")]
        [Authorize(Roles = ("Admin, ZooTrainer, Manager"))]
        public async Task<IActionResult> RemoveMenu(Guid id)
        {
            try
            {
                var result = await _menuService.RemoveMenu(id);
                return Ok(result);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}
