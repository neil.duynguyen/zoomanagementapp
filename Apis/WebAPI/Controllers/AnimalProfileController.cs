﻿using Application.Interfaces;
using Application.ViewModels.AnimalProfileViewModels;
using Domain.Entities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using WebAPI.Validations.AnimalProfileViewModelValidations;

namespace WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]

    public class AnimalProfileController : Controller
    {
        private readonly IAnimalProfileService _animalProfileService;
        private readonly IPieceService _pieceService;

        public AnimalProfileController(IAnimalProfileService animalProfileService, IPieceService pieceService)
        {
            _animalProfileService = animalProfileService;
            _pieceService = pieceService;
        }

        [HttpGet]
        public async Task<IActionResult> GetAnimalProfile()
        {
            try
            {
                var result = await _animalProfileService.GetAnimalProfile();

                /* if (!result.IsNullOrEmpty())
                     return Ok(result);*/

                return Ok(result);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }

        }


        [HttpGet("Search")]
        public async Task<IActionResult> SearchAnimalProfile([FromQuery]SearchAnimalProfileViewModel model)
        {
            try
            { 
                var result = await _animalProfileService.SearchAnimalProfile(model);

                /* if (!result.IsNullOrEmpty())
                     return Ok(result);*/

                return Ok(result);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }

        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetAnimalProfileById(Guid id)
        {
            try
            {
                var result = await _animalProfileService.GetAnimalProfileById(id);

                if (result is not null)
                    return Ok(result);

                return NotFound("Không tìm thấy hồ sơ thú.");
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPost]
        [Authorize(Roles = ("Admin, Manager"))]
        public async Task<IActionResult> CreateAnimalProfile(CreateAnimalProfileViewModel model)
        {
            try
            {
                var piece = await _pieceService.GetPieceById(model.PieceId);

                var validator = new CreateAnimalProfileViewModelValidation();
                var valResult = await validator.ValidateAsync(model);
                if (model.Weight > piece.MaxWeight)
                    return BadRequest("Cân nặng thú không thể vượt quá cân nặng loài");

                if (valResult.Errors.Count != 0)
                {
                    List<string> errors = new List<string>();
                    foreach (var error in valResult.Errors)
                    {
                        var item = error.ErrorMessage; errors.Add(item);
                    }
                    return BadRequest(errors);
                }

                var result = await _animalProfileService.CreateAnimalProfile(model);

                return Ok(result);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }


        }

        [HttpPut]
        [Authorize(Roles = ("Admin, Manager, ZooTrainer"))]
        public async Task<IActionResult> UpdateAnimalProfile(UpdateAnimalProfileViewModel model)
        {
            try
            {
                var piece = await _pieceService.GetPieceById(model.PieceId);

                var validator = new UpdateAnimalProfileViewModelValidation();
                var valResult = await validator.ValidateAsync(model);
                if (model.Weight > piece.MaxWeight)
                    return BadRequest("Cân nặng thú không thể vượt quá cân nặng loài");

                if (valResult.Errors.Count != 0)
                {
                    List<string> errors = new List<string>();
                    foreach (var error in valResult.Errors)
                    {
                        var item = error.ErrorMessage; errors.Add(item);
                    }
                    return BadRequest(errors);
                }

                var result = await _animalProfileService.UpdateAnimalProfile(model);

                return Ok(result);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }

        }

        [HttpDelete]
        [Authorize(Roles = ("Admin, Manager"))]
        public async Task<IActionResult> RemoveAnimalProfile(Guid id)
        {
            try
            {
                var result = await _animalProfileService.RemoveAnimalProfile(id);
                return Ok(result);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }

        }

        [HttpPut("ChangeCage")]
        [Authorize(Roles = ("Admin, Manager"))]
        public async Task<IActionResult> ChangeCage(ChangeCageViewModel model) 
        { 
            try
            {
                var result = await _animalProfileService.ChangeCage(model);

                return Ok(result);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }

        }

    }
}
