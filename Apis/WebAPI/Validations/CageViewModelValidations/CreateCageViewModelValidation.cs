﻿using Application.ViewModels.CageViewModels;
using FluentValidation;

namespace WebAPI.Validations.CageViewModelValidations
{
    public class CreateCageViewModelValidation : AbstractValidator<CreateCageViewModel>
    {
        public CreateCageViewModelValidation()
        {

            RuleFor(x => x.Name)
                .NotNull().WithMessage("Tên chuồng không thể bỏ trống");
                //.Must(BeUniqueName).WithMessage("Tên chuồng không được trùng.");


            RuleFor(x => x.Square)
                .NotNull().WithMessage("Diện tích chuồng không thể bỏ trống")
                .GreaterThan(0).WithMessage("Diện tích chuồng phải lớn hơn 0")
                .LessThan(double.MaxValue).WithMessage("Hãy kiểm tra lại giá trị diện tích chuồng bạn vừa nhập");

        }

        /*public bool BeUniqueName(string name)
        {
            var context = ServiceLocator.Current.GetInstance<AppDbContext>();
            return !context.Cage.Any(x => x.Name == name);
        }*/

    }
}
