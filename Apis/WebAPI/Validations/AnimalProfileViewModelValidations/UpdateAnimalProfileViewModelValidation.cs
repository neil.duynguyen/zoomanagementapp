﻿using Application.ViewModels.AnimalProfileViewModels;
using FluentValidation;

namespace WebAPI.Validations.AnimalProfileViewModelValidations
{
    public class UpdateAnimalProfileViewModelValidation : AbstractValidator<UpdateAnimalProfileViewModel>
    {
        public UpdateAnimalProfileViewModelValidation()
        {
            RuleFor(x => x.Weight)
                .NotNull().WithMessage("Cân nặng thú không thể bỏ trống")
                .GreaterThan(0).WithMessage("Cân nặng thú phải lớn hơn 0");

            RuleFor(x => x.BirthDay)
                .NotNull().WithMessage("Năm sinh thú không thể bỏ trống");
                //.GreaterThan(0).WithMessage("Tuổi thú phải lớn hơn 0");
        }
    }
}
