﻿using Application.ViewModels.RequestViewModels;
using Domain.Entities;
using FluentValidation;

namespace WebAPI.Validations.RequestViewModelValidations
{
    public class CreateRequestViewModelValidation : AbstractValidator<CreateRequestViewModel>
    {
        public CreateRequestViewModelValidation() 
        {
            RuleFor(x => x.RequestType)
                .NotNull().WithMessage("Loại yêu cầu không thể bỏ trống");

            RuleFor(x => x.Description)
                .MaximumLength(1000).WithMessage("Mô tả không thể vượt quá 1000 ký tự");

            RuleFor(x => x.Detail)
                .MaximumLength(1000).WithMessage("Chi tiết không thể vượt quá 1000 ký tự");
        }
    }
}
