﻿using Application.ViewModels.RequestViewModels;
using FluentValidation;

namespace WebAPI.Validations.RequestViewModelValidations
{
    public class UpdateRequestViewModelValidation : AbstractValidator<UpdateStatusRequestViewModel>
    {
        public UpdateRequestViewModelValidation() 
        {
            RuleFor(x => x.Status)
                .NotNull().WithMessage("Loại yêu cầu không thể bỏ trống");
        }
    }
}
