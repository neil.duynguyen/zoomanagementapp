﻿using Application.Interfaces;
using Application.ViewModels.MenuViewModels;
using FluentValidation;

namespace WebAPI.Validations.MenuViewModelValidations
{
    public class UpdateMenuViewModelValidation : AbstractValidator<UpdateMenuViewModel>
    {
        private readonly ICurrentTime _currentTime;
        public UpdateMenuViewModelValidation(ICurrentTime currentTime)
        {
            _currentTime = currentTime;
            RuleFor(x => x.ExpectedQuantity).GreaterThan(0).WithMessage("Số lượng thức ăn dự kiến phải lớn hơn 0").NotNull().WithMessage("Số lượng thức ăn dự kiến không thể bỏ trống."); ;
        }
    }
}
