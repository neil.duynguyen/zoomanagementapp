﻿using Application.Interfaces;
using Application.Services;
using Application.ViewModels.MenuViewModels;
using FluentValidation;
using Microsoft.AspNetCore.Http;
using System.Globalization;
using static System.Runtime.InteropServices.JavaScript.JSType;

namespace WebAPI.Validations.MenuViewModelValidations
{
    public class CreateMenuViewModelValidation : AbstractValidator<CreateMenuViewModel>
    {
        private readonly ICurrentTime _currentTime;
        public CreateMenuViewModelValidation(ICurrentTime currentTime)
        {
            _currentTime = currentTime;
            string formattedDate = _currentTime.GetCurrentTime().ToString("yyyy,MM,dd");
            DateTime date;
            if(DateTime.TryParseExact(formattedDate, "yyyy,MM,dd", null, DateTimeStyles.None, out date))

            RuleFor(x => x.Name).NotNull().WithMessage("Tên thực đơn không được để trống");
            //RuleFor(x => x.Slot).GreaterThan(0).WithMessage("Bữa ăn không được để trống");
            RuleFor(x => x.StartDate).NotNull().WithMessage("Ngày bắt đầu không được để trống");
            RuleFor(x => x.ExpectedQuantity).GreaterThan(0).WithMessage("Số lượng thức ăn dự kiến phải lớn hơn 0").NotNull().WithMessage("Số lượng thức ăn dự kiến không thể bỏ trống."); ;
            RuleFor(x => x.StartDate).GreaterThanOrEqualTo(date).WithMessage("Ngày bắt đầu phải lớn hơn hoặc bằng ngày hiện tại");
        }
    }
}
