﻿using Application.ViewModels.MealViewModels;
using FluentValidation;

namespace WebAPI.Validations.MealViewModelValidations
{
    public class CreateMealViewModelValidation : AbstractValidator<CreateMeal>
    {
        public CreateMealViewModelValidation()
        {
            RuleFor(x => x.MenuId).NotEmpty().NotEmpty().WithMessage("MenuID không được để trống.");
            RuleFor(x => x.ActualQuantily).GreaterThan(0).NotNull().WithMessage("Số lượng thực tế phải lớn hơn 0.");
        }
    }
}
