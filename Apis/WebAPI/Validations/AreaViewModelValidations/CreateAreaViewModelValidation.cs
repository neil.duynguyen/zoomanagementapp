﻿using Application.ViewModels.AreaViewModels;
using FluentValidation;

namespace WebAPI.Validations.AreaViewModelValidations
{
    public class CreateAreaViewModelValidation : AbstractValidator<CreateAreaViewModel>
    {
        public CreateAreaViewModelValidation()
        {
            RuleFor(x => x.Name)
                .NotNull().WithMessage("Tên khu vực không thể để trống");
        }
    }
}