﻿using Application.ViewModels.PieceViewModels;
using FluentValidation;

namespace WebAPI.Validations.PieceViewModelValidations
{
    public class CreatePieceViewModelValidation : AbstractValidator<CreatePieceViewModel>
    {
        public CreatePieceViewModelValidation() 
        { 
            RuleFor(x => x.Name)
                .NotEmpty().WithMessage("Tên loài không thể để trống");
        } 
    }

}
