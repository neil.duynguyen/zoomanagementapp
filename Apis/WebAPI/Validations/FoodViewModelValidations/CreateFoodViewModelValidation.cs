﻿using Application;
using Application.ViewModels.FoodViewModels;
using FluentValidation;
using System.Data;

namespace WebAPI.Validations.FoodViewModelValidations
{
    public class CreateFoodViewModelValidation : AbstractValidator<CreateFoodViewModel>
    {
        public CreateFoodViewModelValidation()
        {
            RuleFor(x => x.Quantity).GreaterThan(0).WithMessage("Số lượng phải lớn hơn 0.").NotNull().WithMessage("Số lượng không thể bỏ trống.");
            RuleFor(x => x.Name).NotNull().WithMessage("Tên thực phẩm không được để trống.");
            RuleFor(x => x.Description).NotNull().WithMessage("Mô tả thực phẩm không được để trống.");
        }

    }
}
