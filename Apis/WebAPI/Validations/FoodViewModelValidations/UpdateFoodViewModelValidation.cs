﻿using Application.ViewModels.AnimalProfileViewModels;
using Application.ViewModels.FoodViewModels;
using FluentValidation;

namespace WebAPI.Validations.FoodViewModelValidations
{
    public class UpdateFoodViewModelValidation : AbstractValidator<UpdateFoodViewModel>
    {
        public UpdateFoodViewModelValidation()
        {
            RuleFor(x => x.Quantity).GreaterThan(0).WithMessage("Số lượng phải lớn hơn 0.").NotNull().WithMessage("Số lượng không thể bỏ trống.");
            RuleFor(x => x.Description).NotNull().WithMessage("Mô tả thực phẩm không được để trống.");
        }
    }
}
