﻿using Domain.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Entities
{
    public class AnimalProfile : BaseEntity
    {
        [ForeignKey("User")]
        public Guid UserId { get; set; }
        [ForeignKey("Cage")]
        public Guid CageId { get; set; }
        
        [ForeignKey("Piece")]
        public Guid PieceId { get; set; }
        public string? Name { get; set; }

        public double Weight { get; set; }
        public string? Image { get; set; }
        public string? BirthCertificated { get; set; }
        public DateTime BirthDay { get; set; }
        public string ProfileNumber { get; set; }
        public AnimalStatus AnimalStatus { get; set; }

        public virtual User User { get; set; }
        public virtual Cage Cage { get; set; }
        public virtual Piece Piece { get; set; }
        public IList<ProfileHistory> ProfileHistories { get; set; }
        public IList<Menu> Menus { get; set; }
        
    }
}
