﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Entities
{
    public class Meal : BaseEntity
    {
        [ForeignKey("Menu")]
        public Guid MenuId { get; set; }   
        public DateTime Date { get; set; }
        
        public double ActualQuantily { get; set; }
        public virtual Menu Menu { get; set; }
    }
}
