﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Entities
{
    public class Cage : BaseEntity
    {
        [ForeignKey("Area")]
        public Guid AreaId { get; set; }
        public string CageNumber { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public double Square { get;set; }
        public int Capacity { get; set; }
        public virtual Area Area { get; set; }
        public IList<AnimalProfile> AnimalProfiles { get; set; }
    }
}
