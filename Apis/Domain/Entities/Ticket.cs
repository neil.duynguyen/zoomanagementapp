﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Entities
{
    public class Ticket : BaseEntity
    {
        public double Price { get; set; }
        public string TicketType { get; set; }
        public IList<OrderDetail> OrderDetails { get; set; }
    }
}
