﻿using Domain.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Entities
{
    public class Request : BaseEntity
    {
        [ForeignKey("User")]
        public Guid UserId { get; set; }
        public string RequestType { get; set; }
        public string Description { get; set; }
        public string Detail { get; set; }
        public RequestStatus RequestStatus { get; set; }
        public virtual User User { get; set; }
    }
}
