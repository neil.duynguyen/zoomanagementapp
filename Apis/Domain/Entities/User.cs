﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Domain.Entities
{
    public class User : BaseEntity
    {
        [ForeignKey("Role")]
        public Guid RoleId { get; set; }
        public string UserName { get; set; }
        public string FullName { get; set; }
        public string Email { get; set; }
        public string? Avatar { get; set; }
        public string PasswordHash { get; set; }
        public string? Phone { get; set; }
        public string? OTP { get; set; }

        public virtual Role Role { get;set; }
        public IList<Area> Areas { get; set; }
        public IList<Article> Articles { get; set; }
        public IList<AnimalProfile> AnimalProfiles { get; set; }
        public IList<Order> Orders { get; set; }
        public IList<Request> Requests { get; set; }
    }
}
