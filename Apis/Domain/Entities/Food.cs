﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Entities
{
    public class Food : BaseEntity
    {
        public string Name { get; set; }
        public double Quantity { get; set; }
        public string? Description { get; set; }
        public IList<Menu> Menus { get; set; }
    }
}
