﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Entities
{
    public class Piece : BaseEntity
    {
        public string Name { get; set; }
        public double MaxWeight { get; set; }
        public IList<AnimalProfile> AnimalProfiles { get; set; }
    }
}
