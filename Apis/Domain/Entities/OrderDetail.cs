﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Entities
{
    public class OrderDetail : BaseEntity
    {
        [ForeignKey("Order")]
        public Guid OrderId { get; set; }
        [ForeignKey("Ticket")]
        public Guid TicketId { get; set; }
        public int Quantity { get; set; }
        public double TicketPrice { get; set; }
        public string? QrCode { get; set; }
        public double Total { get; set; }       
        public virtual Ticket Ticket { get; set; }
        public virtual Order Order { get; set; }
    }
}
