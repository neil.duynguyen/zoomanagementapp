﻿using Domain.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Entities
{
    public class Order : BaseEntity
    {
        [ForeignKey("User")]
        public Guid UserId { get; set; }
        public string OrderNumber { get; set; }
        public double TotalAmount { get; set; }
        public string? PaymentMethod { get; set; }
        public OrderStatus OrderStatus { get; set; }
        public DateTime? CheckInDate { get; set; }
        public IList<OrderDetail> OrderDetails { get; set; }
        public virtual User User { get; set; }
    }
}
