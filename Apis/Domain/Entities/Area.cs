﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Entities
{
    public class Area : BaseEntity
    {
        [ForeignKey("User")]
        public Guid UserId { get; set; }
        public string Name { get; set; }
        public IList<Cage> Cages { get; set; }
        public virtual User User { get; set; }
    }
}
