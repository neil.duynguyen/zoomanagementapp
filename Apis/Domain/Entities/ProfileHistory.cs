﻿using Domain.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Entities
{
    public class ProfileHistory : BaseEntity
    {
        [ForeignKey("AnimalProfile")]
        public Guid AnimalProfileId { get; set; }
        public Guid CurrentCageId { get; set; }
        public string Description { get; set; }
        public AnimalStatus AnimalStatus { get; set; }
        public virtual AnimalProfile AnimalProfile { get; set; }
    }
}
