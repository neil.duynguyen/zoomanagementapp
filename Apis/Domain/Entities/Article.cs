﻿using Domain.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Entities
{
    public class Article : BaseEntity
    {
        [ForeignKey("User")]
        public Guid UserId { get; set; }
        public string Title { get; set; }
        public string Body { get; set; }
        public string Thumbnail { get;set; }
        public ArticleStatus ArticleStatus { get; set; }
        public virtual User User { get; set; }
    }
}
