﻿using Domain.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Entities
{
    public class Menu : BaseEntity
    {
        [ForeignKey("Food")]
        public Guid FoodId { get; set; }
        [ForeignKey("AnimalProfile")]
        public Guid AnimalProfileId { get; set; }
        public string Name { get; set; }
        public Slot Slot { get;set; }

        public DateTime? StartDate { get; set; }
        public double ExpectedQuantity { get; set; }
        public virtual Food Food { get; set; }
        public virtual AnimalProfile AnimalProfile { get; set; }
        public IList<Meal> Meals { get; set; }
    }
}
