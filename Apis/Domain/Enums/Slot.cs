﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Enums
{
    public enum Slot
    {
        Breakfast = 1,
        Lunch = 2,
        Afternoon = 3
    }
}
