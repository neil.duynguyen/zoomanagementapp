﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Enums
{
    public enum OrderStatus
    {
        Paid = 1, 
        Failed = 2,
        Unpaid = 3,
        Waiting = 4
    }
}
