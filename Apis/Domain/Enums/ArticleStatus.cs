﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Enums
{
    public enum ArticleStatus
    {
        Created = 1,
        Publish = 2,
        Unpublished = 3,
    }
}
