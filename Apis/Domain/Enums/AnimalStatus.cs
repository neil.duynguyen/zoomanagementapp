﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Enums
{
    public enum AnimalStatus
    {
        OnChanging = 1,
        New = 2,
        Updated = 3,
        IsRemove = 4,
    }
}
