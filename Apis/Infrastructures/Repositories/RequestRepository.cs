﻿using Application.Interfaces;
using Application.Repositories;
using Domain.Entities;
using Microsoft.EntityFrameworkCore;

namespace Infrastructures.Repositories
{
    public class RequestRepository : GenericRepository<Request>, IRequestRepository
    {
        private readonly AppDbContext _appDbContext;
        public RequestRepository(AppDbContext context, ICurrentTime timeService, IClaimsService claimsService) : base(context, timeService, claimsService)
        {
            _appDbContext = context;
        }

        public async Task<List<Request>> GetRequestByCreateDate(string createDate)
        {
            var request = await _appDbContext.Request
                .Where(x => x.CreationDate < (DateTime.Parse(createDate).AddHours(24)) && x.IsDeleted == false).ToListAsync();
            if (request != null)
            {
                return request;
            }
            throw new Exception("Không tìm thấy yêu cầu");
        }
    }
}
