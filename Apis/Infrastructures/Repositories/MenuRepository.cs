﻿using Application.Interfaces;
using Application.Repositories;
using Application.ViewModels.AnimalProfileViewModels;
using Application.ViewModels.MenuViewModels;
using Domain.Entities;
using Microsoft.EntityFrameworkCore;

namespace Infrastructures.Repositories
{
    public class MenuRepository : GenericRepository<Menu>, IMenuRepository
    {
        private readonly AppDbContext _dbContext;
        public MenuRepository(AppDbContext context, ICurrentTime timeService, IClaimsService claimsService) : base(context, timeService, claimsService)
        {
            _dbContext = context;
        }
        public async Task<List<Menu>> SearchMenu(SearchMenuModel model)
        {
            var searchResult = await _dbContext.Menu
                .Include(x => x.AnimalProfile)
                .Include(x => x.Food)
                .Include(x => x.Meals)
                .Where(x => x.Name.Contains(model.Name)
            || x.StartDate.Value == model.StartDate
            || x.AnimalProfile.ProfileNumber.Contains(model.ProfileNumber)
            || x.Slot.Equals(model.Slot)
            ).ToListAsync();

            return searchResult;
        }

        public override async Task<Menu> GetByIdAsync(Guid id)
        {
            return await _dbContext.Menu.Include(x => x.Food).Include(x => x.AnimalProfile).FirstOrDefaultAsync(x => x.Id == id);
        }

        public override async Task<List<Menu>> GetAllAsync()
        {
            return await _dbContext.Menu.Include(x => x.Food).Include(x => x.AnimalProfile).Where(x => !x.IsDeleted).ToListAsync();
        }
    }
}
