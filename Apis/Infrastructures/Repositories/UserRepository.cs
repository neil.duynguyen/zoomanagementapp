﻿using Application.Interfaces;
using Application.Repositories;
using Application.ViewModels.UserViewModels;
using Domain.Entities;
using Microsoft.EntityFrameworkCore;

namespace Infrastructures.Repositories
{
    public class UserRepository : GenericRepository<User>, IUserRepository
    {
        private readonly AppDbContext _dbContext;

        public UserRepository(AppDbContext dbContext,ICurrentTime timeService, IClaimsService claimsService) : base(dbContext, timeService, claimsService)
        {
            _dbContext = dbContext;
        }

        public Task<bool> CheckUserNameExited(string username) => _dbContext.Users.AnyAsync(u => u.UserName == username);

        public override async Task<List<User>> GetAllAsync()
        {
            return await _dbSet.Include(u => u.Role).Where(x => !x.IsDeleted).ToListAsync();
        }
        public override async Task<User> GetByIdAsync(Guid id)
        {
            return await _dbSet.Include(u => u.Role).FirstOrDefaultAsync(x => x.Id == id);
        }


        public async Task<User> GetUserByUserNameAndPasswordHash(string username, string passwordHash)
        {
            var user = await _dbContext.Users.Include(u => u.Role)
                .FirstOrDefaultAsync( record => record.UserName == username
                                        && record.PasswordHash == passwordHash);
           /* if(user is null)
            {
                throw new Exception("Tên người đăng nhập hoặc mật khẩu không chính xác.");
            }*/
            return user;
        }

        public async Task<User> GetUserByEmail(string email)
        {
            var user = await _dbContext.Users.FirstOrDefaultAsync(x => x.Email == email);
            return user;
        }

        /*public async Task<User> GetUserById(Guid? id)
        {
            var user = await _dbContext.Users.FirstOrDefaultAsync(x => x.Id == id);
            return user;
        }*/

    }
}
