﻿using Application.Interfaces;
using Application.Repositories;
using Application.ViewModels.AnimalProfileViewModels;
using Application.ViewModels.ArticleViewModels;
using Domain.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructures.Repositories
{
    public class ArticleRepository : GenericRepository<Article>, IArticleRepository
    {
        private readonly AppDbContext _appDbContext;

        public ArticleRepository(AppDbContext context, ICurrentTime timeService, IClaimsService claimsService) : base(context, timeService, claimsService)
        {
            _appDbContext = context;
        }

        public async Task<List<Article>> SearchArticle(SearchArticleViewModel model)
        {
            var searchResult = await _appDbContext.Article
                .Include(x => x.User)
                .Where(x => x.User.FullName.Contains(model.FullUserName)
            || x.Title.Contains(model.Title)
            || x.Body.Contains(model.Body)
            || x.CreationDate.Date >= model.CreateDate
            || x.ArticleStatus.Equals(model.ArticleStatus)
            || x.User.Id == model.UserId
            ).ToListAsync();

            return searchResult;
        }

        public override async Task<Article> GetByIdAsync(Guid id)
        {
            return await _appDbContext.Article.Include(x => x.User).FirstOrDefaultAsync(x => x.Id == id);
        }
        public override async Task<List<Article>> GetAllAsync()
        {
            return await _appDbContext.Article.Include(x => x.User).ToListAsync();
        }
    }
}
