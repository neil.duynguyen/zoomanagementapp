﻿using Application.Interfaces;
using Application.Repositories;
using Application.ViewModels.MealViewModels;
using Domain.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructures.Repositories
{
    public class MealRepository : GenericRepository<Meal>, IMealRepository
    {
        private readonly AppDbContext _dbContext;
        public MealRepository(AppDbContext context, ICurrentTime timeService, IClaimsService claimsService) : base(context, timeService, claimsService)
        {
            _dbContext = context;
        }

        public override async Task<Meal> GetByIdAsync(Guid id)
        {
            return await _dbContext.Meal.Include(x => x.Menu).FirstOrDefaultAsync(x => x.Id == id);
        }

        public override async Task<List<Meal>> GetAllAsync()
        {
            return await _dbContext.Meal.Include(x => x.Menu).Include(x => x.Menu).Where(x => !x.IsDeleted).ToListAsync();
        }
    }
}
