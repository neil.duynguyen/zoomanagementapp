﻿using Application.Interfaces;
using Application.Repositories;
using Application.ViewModels.AnimalProfileViewModels;
using Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructures.Repositories
{
    public class AnimalProfileRepository : GenericRepository<AnimalProfile>, IAnimalProfileRepository
    {
        private readonly AppDbContext _appDbContext;
        public AnimalProfileRepository(AppDbContext context, ICurrentTime timeService, IClaimsService claimsService) : base(context, timeService, claimsService)
        {
            _appDbContext = context;
        }

        public AnimalProfile getAnimalProfileByIdWithRelation(Guid id)
        {
            var animalProfile = _appDbContext.AnimalProfile
                .Include(x => x.Cage)
                .Include(x => x.Piece)
                .Include(x => x.User)
                .Where(x => x.Id == id).FirstOrDefault();
            if (animalProfile != null)
            {
                return animalProfile;
            }
            throw new Exception("Không tìm thấy hồ sơ thú");
        }

        public async Task<List<AnimalProfile>> SearchAnimalProfile(SearchAnimalProfileViewModel model)
        {
            var searchResult = await _appDbContext.AnimalProfile
                .Include(x => x.User)
                .Include(x => x.Cage)
                .Include(x => x.Piece)
                .Include(x => x.ProfileHistories)
                .Where(x => x.User.FullName.Contains(model.FullUserName)
            || x.Cage.CageNumber.Equals(model.CageNumber)
            || x.PieceId == model.PieceId
            || x.Name.Contains(model.AnimalName)
            || x.Weight == model.Weight
            || x.BirthDay.Date == model.BirthDay
            || x.ProfileNumber.Contains(model.ProfileNumber)
            || x.AnimalStatus.Equals(model.AnimalStatus)
            ).ToListAsync();

            return searchResult;
        }

        public override async Task<List<AnimalProfile>> GetAllAsync()
        {
            return await _appDbContext.AnimalProfile
                .Include(x => x.User)
                .Include(x => x.Cage)
                .Include(x => x.Piece)
                .Include(x => x.ProfileHistories)
                .Include(x => x.Menus)
                .ToListAsync();        
        }

        public override async Task<AnimalProfile> GetByIdAsync(Guid id)
        {
            return await _appDbContext.AnimalProfile
                .Include(x => x.User)
                .Include(x => x.Cage)
                .Include(x => x.Piece)
                .Include(x => x.ProfileHistories)
                .Include(x => x.Menus)
                .FirstOrDefaultAsync(x => x.Id == id);
        }
    }
}
