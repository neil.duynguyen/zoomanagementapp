﻿using Application.Interfaces;
using Application.Repositories;
using Domain.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructures.Repositories
{
    public class PieceRepository : GenericRepository<Piece>, IPieceRepository
    {
        private readonly AppDbContext _appDbContext;
        public PieceRepository(AppDbContext context, ICurrentTime timeService, IClaimsService claimsService) : base(context, timeService, claimsService)
        {
            _appDbContext = context;
        }

        public Piece getPieceByIdWithRelation(Guid id)
        {
            var piece = _appDbContext.Piece.Include(x => x.AnimalProfiles).Where(x => x.Id == id).FirstOrDefault();
            if (piece != null)
            {
                return piece;
            }
            throw new Exception("Không tìm thấy loài");
        }
    }
}
