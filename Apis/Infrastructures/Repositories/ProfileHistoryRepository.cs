﻿

using Application.Interfaces;
using Application.Repositories;
using Domain.Entities;
using Microsoft.EntityFrameworkCore;

namespace Infrastructures.Repositories
{
    public class ProfileHistoryRepository : GenericRepository<ProfileHistory>, IProfileHistoryRepository
    {
        private readonly AppDbContext _appDbContext;
        public ProfileHistoryRepository(AppDbContext context, ICurrentTime timeService, IClaimsService claimsService) : base(context, timeService, claimsService)
        {
            _appDbContext = context;
        }

        public ProfileHistory getProfileHistoryByIdWithRelation(Guid id)
        {
            var profileHistory = _appDbContext.ProfileHistories
                .Include(x => x.AnimalProfile)
                .ThenInclude(x => x.User)
                .Where(x => x.Id == id).FirstOrDefault();
            if (profileHistory != null)
            {
                return profileHistory;
            }
            throw new Exception("không tìm thấy lịch sử hồ sơ");
        }

        public override async Task<List<ProfileHistory>> GetAllAsync()
        {
            return await _appDbContext.ProfileHistories.Include(x => x.AnimalProfile).ThenInclude(x => x.User).Where(x => !x.IsDeleted).ToListAsync();
        }
    }
}
