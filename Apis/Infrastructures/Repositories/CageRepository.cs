﻿using Application.Interfaces;
using Application.Repositories;
using Domain.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructures.Repositories
{
    public class CageRepository :GenericRepository<Cage>, ICageRepository
    {
        private readonly AppDbContext _appDbContext;
        public CageRepository(AppDbContext context, ICurrentTime timeService, IClaimsService claimsService) : base(context, timeService, claimsService)
        {
            _appDbContext = context;
        }

        public Cage getCageByIdWithRelation(Guid id)
        {
            var cage = _appDbContext.Cage.Include(x => x.AnimalProfiles).Where(x => x.Id == id).FirstOrDefault();
            if (cage != null)
            {
                return cage;
            }
            throw new Exception("không tìm thấy chuồng");
        }

        public override async Task<Cage> GetByIdAsync(Guid id)
        {
            return await _appDbContext.Cage.Include(x => x.Area).FirstOrDefaultAsync(x => x.Id == id);
        }

        public override async Task<List<Cage>> GetAllAsync()
        {
            return await _appDbContext.Cage.Include(x => x.Area).ToListAsync();
        }
    }
}
