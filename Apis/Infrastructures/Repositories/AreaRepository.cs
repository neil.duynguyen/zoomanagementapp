﻿using Application.Interfaces;
using Application.Repositories;
using Domain.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructures.Repositories
{
    public class AreaRepository :GenericRepository<Area>, IAreaRepository
    {
        private readonly AppDbContext _appDbContext;
        public AreaRepository(AppDbContext context, ICurrentTime timeService, IClaimsService claimsService) : base(context, timeService, claimsService)
        {
            _appDbContext = context;
        }

        public Area getAreaByIdWithRelation(Guid id)
        {
            var area = _appDbContext.Area.Include(x => x.Cages).Where(x => x.Id == id).FirstOrDefault();
            if (area != null)
            {
                return area;
            }
            throw new Exception("Không tìm thấy khu vực");
        }

        public override async Task<List<Area>> GetAllAsync()
        {
            return await _appDbContext.Area.Include(x => x.User).ToListAsync();
        }

        public override async Task<Area> GetByIdAsync(Guid id)
        {
            return await _appDbContext.Area.Include(x => x.User).FirstOrDefaultAsync(x => x.Id == id);
        }
    }
}
