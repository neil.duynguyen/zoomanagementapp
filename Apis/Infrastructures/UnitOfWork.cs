﻿using Application;
using Application.Repositories;

namespace Infrastructures
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly AppDbContext _dbContext;
        private readonly IUserRepository _userRepository;
        private readonly IRoleRepository _roleRepository;
        private readonly IPieceRepository _pieceRepository;
        private readonly IMenuRepository _menuRepository;
        private readonly IAreaRepository _areaRepository;
        private readonly IFoodRepository _foodRepository;
        private readonly ITicketRepository _ticketRepository;
        private readonly IArticleRepository _articleRepository;
        private readonly ICageRepository _cageRepository;
        private readonly IAnimalProfileRepository _animalProfileRepository;
        private readonly IProfileHistoryRepository _profileHistoryRepository;
        private readonly IMealRepository _mealRepository;
        private readonly IRequestRepository _requestRepository;
        private readonly IOrderRepository _orderRepository;
        private readonly IOrderDetailRepository _orderDetailRepository;

        public UnitOfWork(AppDbContext dbContext,
            IUserRepository userRepository,
            IRoleRepository roleRepository,
            IPieceRepository pieceRepository,
            IMenuRepository menuRepository,
            IAreaRepository areaRepository,
            IFoodRepository foodRepository,
            ITicketRepository ticketRepository,
            IArticleRepository articleRepository,
            ICageRepository cageRepository,
            IAnimalProfileRepository animalProfileRepository,
            IProfileHistoryRepository profileHistoryRepository,
            IMealRepository mealRepository,
            IRequestRepository requestRepository,
            IOrderRepository orderRepository,
            IOrderDetailRepository orderDetailRepository)
        {
            _dbContext = dbContext;
            _userRepository = userRepository;
            _roleRepository = roleRepository;
            _pieceRepository = pieceRepository;
            _menuRepository = menuRepository;
            _areaRepository = areaRepository;
            _foodRepository = foodRepository;
            _ticketRepository = ticketRepository;
            _articleRepository = articleRepository;
            _cageRepository = cageRepository;
            _animalProfileRepository = animalProfileRepository;
            _profileHistoryRepository = profileHistoryRepository;
            _mealRepository = mealRepository;
            _requestRepository = requestRepository;
            _orderRepository = orderRepository;
            _orderDetailRepository = orderDetailRepository;
        }
        public IUserRepository UserRepository => _userRepository;

        public IRoleRepository RoleRepository => _roleRepository;

        public IPieceRepository PieceRepository => _pieceRepository;

        public IMenuRepository MenuRepository => _menuRepository;

        public IAreaRepository AreaRepository => _areaRepository;

        public IFoodRepository FoodRepository => _foodRepository;
        public ITicketRepository TicketRepository => _ticketRepository;

        public IArticleRepository ArticleRepository => _articleRepository;
        public ICageRepository CageRepository => _cageRepository;
        public IAnimalProfileRepository AnimalProfileRepository => _animalProfileRepository;
        public IProfileHistoryRepository ProfileHistoryRepository => _profileHistoryRepository;

        public IMealRepository MealRepository => _mealRepository;
        public IRequestRepository RequestRepository => _requestRepository;

        public IOrderRepository OrderRepository => _orderRepository;

        public IOrderDetailRepository OrderDetailRepository => _orderDetailRepository;

        public async Task<int> SaveChangeAsync()
        {
            return await _dbContext.SaveChangesAsync();
        }
    }
}
