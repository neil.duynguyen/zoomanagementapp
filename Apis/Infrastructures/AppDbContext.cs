﻿using Application.Utils;
using Domain.Entities;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.Extensions.Configuration;
using System.Reflection.Emit;
using System.Security.AccessControl;

namespace Infrastructures
{
    public class AppDbContext : DbContext
    {
        public AppDbContext() { }
        public AppDbContext(DbContextOptions<AppDbContext> options) : base(options)
        {

        }
        public DbSet<User> Users { get; set; }
        public DbSet<Area> Area { get; set; }
        public DbSet<Article> Article { get; set; }
        public DbSet<AnimalProfile> AnimalProfile { get; set; }
        public DbSet<Cage> Cage { get; set; }
        public DbSet<Food> Food { get; set; }
        public DbSet<Meal> Meal { get; set; }
        public DbSet<Menu> Menu { get; set; }
        public DbSet<Notification> Notification { get; set; }
        public DbSet<Order> Order { get; set; }
        public DbSet<OrderDetail> OrderDetail { get; set; }
        public DbSet<Piece> Piece { get; set; }
        public DbSet<ProfileHistory> ProfileHistories { get; set; }
        public DbSet<Role> Role { get; set; }
        public DbSet<Ticket> Ticket { get; set; }
        public DbSet<Request> Request { get; set; }


        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.Entity<AnimalProfile>(e =>
            {
                e.HasOne(u => u.User).WithMany(u => u.AnimalProfiles)
                .HasForeignKey(u => u.UserId).OnDelete(DeleteBehavior.ClientSetNull);
            });

            builder.Entity<Role>().HasData(
                new Role { Id = new Guid("D5FA55C7-315D-4634-9C73-08DBBC3F3A50"), Name = "Admin",  CreationDate = new DateTime(2023 - 09 - 30), IsDeleted = false },
                new Role { Id = new Guid("D5FA55C7-315D-4634-9C73-08DBBC3F3A51"), Name = "Staff",  CreationDate = new DateTime(2023 - 09 - 30), IsDeleted = false },
                new Role { Id = new Guid("D5FA55C7-315D-4634-9C73-08DBBC3F3A52"), Name = "Manager",  CreationDate = new DateTime(2023 - 09 - 30), IsDeleted = false },
                new Role { Id = new Guid("D5FA55C7-315D-4634-9C73-08DBBC3F3A53"), Name = "ZooTrainer",  CreationDate = new DateTime(2023 - 09 - 30), IsDeleted = false },
                new Role { Id = new Guid("D5FA55C7-315D-4634-9C73-08DBBC3F3A54"), Name = "User",  CreationDate = new DateTime(2023 - 09 - 30), IsDeleted = false }
                );

            var hashPasswordAdmin = "Admin@123";
            var hashPasswordStaff = "Staff@123";
            var hashPasswordManager = "Manager@123";
            var hashPasswordZootrainer = "Zootrainer@123";
            var hashPasswordUser = "User@123";
            

            builder.Entity<User>().HasData(
                new User { Id = new Guid("3F4AAEF9-FF3C-41EA-DAC2-08DBC4DD144F"), RoleId = new Guid("D5FA55C7-315D-4634-9C73-08DBBC3F3A50"), UserName = "admin", FullName = "Administrator", Email = "admin@gmail.com",PasswordHash = hashPasswordAdmin.Hash(), Phone = "0987654334", CreationDate = new DateTime(2023 - 09 - 30), IsDeleted = false},
                new User { Id = new Guid("3F4AAEF9-FF3C-41EA-DAC2-08DBC4DD145F"), RoleId = new Guid("D5FA55C7-315D-4634-9C73-08DBBC3F3A51"), UserName = "duystaff", FullName = "Nguyen Van Duy", Email = "duynguyen@gmail.com",PasswordHash = hashPasswordStaff.Hash(), Phone = "0987654954", CreationDate = new DateTime(2023 - 09 - 30), IsDeleted = false},
                new User { Id = new Guid("3F4AAEF9-FF3C-41EA-DAC2-08DBC4DD146F"), RoleId = new Guid("D5FA55C7-315D-4634-9C73-08DBBC3F3A52"), UserName = "dungmanager", FullName = "Hoang Quoc Dung", Email = "quocdung@gmail.com",PasswordHash = hashPasswordManager.Hash(), Phone = "0987784334", CreationDate = new DateTime(2023 - 09 - 30), IsDeleted = false},
                new User { Id = new Guid("3F4AAEF9-FF3C-41EA-DAC2-08DBC4DD147F"), RoleId = new Guid("D5FA55C7-315D-4634-9C73-08DBBC3F3A53"), UserName = "zootrainer01", FullName = "Ngo Thanh Ha", Email = "thanhha@gmail.com",PasswordHash = hashPasswordZootrainer.Hash(), Phone = "0357654334", CreationDate = new DateTime(2023 - 09 - 30), IsDeleted = false},
                new User { Id = new Guid("3F4AAEF9-FF3C-41EA-DAC2-08DBC4DD148F"), RoleId = new Guid("D5FA55C7-315D-4634-9C73-08DBBC3F3A53"), UserName = "zootrainer02", FullName = "Pham Thanh Minh", Email = "thanhmihh@gmail.com",PasswordHash = hashPasswordZootrainer.Hash(), Phone = "0897654334", CreationDate = new DateTime(2023 - 09 - 30), IsDeleted = false},
                new User { Id = new Guid("3F4AAEF9-FF3C-41EA-DAC2-08DBC4DD149F"), RoleId = new Guid("D5FA55C7-315D-4634-9C73-08DBBC3F3A54"), UserName = "duynguyen009", FullName = "Nguyen Thanh Duy", Email = "nguyenthanhduy@gmail.com",PasswordHash = hashPasswordUser.Hash(), Phone = "0965154334", CreationDate = new DateTime(2023 - 09 - 30), IsDeleted = false},
                new User { Id = new Guid("3F4AAEF9-FF3C-41EA-DAC2-08DBC4DD143F"), RoleId = new Guid("D5FA55C7-315D-4634-9C73-08DBBC3F3A54"), UserName = "Guest", FullName = "Guest", Email = "Guest", PasswordHash = hashPasswordUser.Hash(), Phone = "", CreationDate = new DateTime(2023 - 09 - 30), IsDeleted = false}
                );

            base.OnModelCreating(builder);
        }

       protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                IConfiguration config = new ConfigurationBuilder()
                                   .SetBasePath(Directory.GetCurrentDirectory())
                                   .AddJsonFile("appsettings.Production.json").Build();

                optionsBuilder.UseSqlServer(config.GetConnectionString("Production"));

            }
        }
    }
}
