﻿using AutoMapper;
using Application.Commons;
using Domain.Entities;
using Application.ViewModels.RoleViewModels;
using Application.ViewModels.MenuViewModels;
using Application.ViewModels.FoodViewModels;
using Application.ViewModels.PieceViewModels;
using Application.ViewModels.AreaViewModels;
using Application.ViewModels.TicketViewModels;
using Application.ViewModels.ArticleViewModels;
using Application.ViewModels.CageViewModels;
using Application.ViewModels.UserViewModels;
using Application.ViewModels.AnimalProfileViewModels;
using Application.ViewModels.ProfileHistoryViewModels;
using Microsoft.IdentityModel.Tokens;
using Application.ViewModels.MealViewModels;
using Application.ViewModels.RequestViewModels;
using Application.ViewModels.OrderViewModels;

namespace Infrastructures.Mappers
{
    public class MapperConfigurationsProfile : Profile
    {
        public MapperConfigurationsProfile()
        {
            CreateMap(typeof(Pagination<>), typeof(Pagination<>));

            CreateMap<RoleViewModel, Role>().ReverseMap();


            CreateMap<Menu, MenuViewModel>()
                .ForMember(des => des.NameFood, src => src.MapFrom(x => x.Food.Name))
                .ForMember(des => des.StartDate, src => src.MapFrom(x => x.StartDate != null ? ((DateTime)x.StartDate - new DateTime(1970, 1, 1)).TotalSeconds : (double?)null))
                .ReverseMap();
            CreateMap<Menu, MenuViewModelDetail>()
                //.ForMember(des => des.NameFood, src => src.MapFrom(x => x.Food.Name))
                .ForMember(des => des.StartDate, src => src.MapFrom(x => x.StartDate != null ? ((DateTime)x.StartDate - new DateTime(1970, 1, 1)).TotalSeconds : (double?)null))
                .ReverseMap();
            CreateMap<CreateMenuViewModel, Menu>().ReverseMap();
            CreateMap<UpdateMenuViewModel, Menu>().ReverseMap();

            CreateMap<Piece, PieceViewModel>().ReverseMap();
            CreateMap<Piece, CreatePieceViewModel>().ReverseMap();
            CreateMap<Piece, UpdatePieceViewModel>().ReverseMap();

            CreateMap<Area, AreaViewModel>().ReverseMap();
            CreateMap<Area, AreaViewModelDetail>().ReverseMap();
            CreateMap<Area, CreateAreaViewModel>().ReverseMap();

            CreateMap<Food, FoodViewModel>().ReverseMap();
            CreateMap<Food, CreateFoodViewModel>().ReverseMap();
            CreateMap<Food, UpdateFoodViewModel>().ReverseMap();

            CreateMap<Ticket, TicketViewModel>().ReverseMap();
            CreateMap<Ticket, CreateTicketViewModel>().ReverseMap();

            CreateMap<Article, ArticleViewModel>().ReverseMap();
            CreateMap<Article, ArticleViewModelDetail>().ReverseMap();
            CreateMap<Article, CreateArticleViewModel>().ReverseMap();
            CreateMap<Article, UpdateArticleViewModel>().ReverseMap();

            CreateMap<Cage, CageViewModel>().ReverseMap();
            CreateMap<Cage, CreateCageViewModel>().ReverseMap();
            CreateMap<Cage, UpdateCageViewModel>().ReverseMap();

            CreateMap<User, UserViewModel>()
                .ForMember(des => des.Role, src => src.MapFrom(x => x.Role.Name)).ReverseMap();
            CreateMap<User, UpdateUserViewModel>().ReverseMap();
            CreateMap<User, UpdateUserRoleViewModel>().ReverseMap();

            CreateMap<AnimalProfile, AnimalProfileViewModel>()
                .ForMember(des => des.BirthDay, src => src.MapFrom(x => x.BirthDay != null ? ((DateTime)x.BirthDay - new DateTime(1970, 1, 1)).TotalSeconds : (double?)null))
                .ForMember(des => des.AnimalStatus, src => src.MapFrom(x => x.AnimalStatus != null ? (string)x.AnimalStatus.ToString() : (string?)null))
                .ReverseMap();
            CreateMap<AnimalProfile, AnimalProfileViewModelDetail>()
                .ForMember(des => des.BirthDay, src => src.MapFrom(x => x.BirthDay != null ? ((DateTime)x.BirthDay - new DateTime(1970, 1, 1)).TotalSeconds : (double?)null))
                .ForMember(des => des.AnimalStatus, src => src.MapFrom(x => x.AnimalStatus != null ? (string)x.AnimalStatus.ToString() : (string?)null))
                .ReverseMap();
            CreateMap<AnimalProfile, CreateAnimalProfileViewModel>().ReverseMap();
            CreateMap<AnimalProfile, UpdateAnimalProfileViewModel>().ReverseMap();

            CreateMap<ProfileHistory, ProfileHistoryViewModel>()
                .ForMember(des => des.AnimalStatus, src => src.MapFrom(x => x.AnimalStatus != null ? (string)x.AnimalStatus.ToString() : (string?)null))
                //.ForMember(des => des.CreatedBy, src => src.MapFrom(x =>  _userRepository.GetUserById(x.CreatedBy)))
                .ReverseMap();
            CreateMap<ProfileHistory, CreateProfileHistoryViewModel>().ReverseMap();

            CreateMap<Meal, MealViewModel>()
                .ForMember(des => des.Date, src => src.MapFrom(x => x.Date != null ? ((DateTime)x.Date - new DateTime(1970, 1, 1)).TotalSeconds : (double?)null))
                .ReverseMap();
            CreateMap<Meal, CreateMeal>().ReverseMap();

            CreateMap<Request, RequestViewModel>()
                .ForMember(des => des.RequestStatus, src => src.MapFrom(x => x.RequestStatus != null ? (string)x.RequestStatus.ToString() : (string?)null))
                .ReverseMap();
            CreateMap<Request, CreateRequestViewModel>().ReverseMap();
            CreateMap<Request, UpdateStatusRequestViewModel>().ReverseMap();

            CreateMap<Order, CreateOrderDetailViewModel>().ReverseMap();
            CreateMap<Order, OrderViewModel>().ReverseMap();
        }
    }
}
