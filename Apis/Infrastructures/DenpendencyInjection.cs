﻿using Application;
using Application.Interfaces;
using Application.Repositories;
using Application.Services;
using Infrastructures.Mappers;
using Infrastructures.Repositories;
using Infrastructures.Services;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Infrastructures
{
    public static class DenpendencyInjection
    {
        public static IServiceCollection AddInfrastructuresService(this IServiceCollection services, IConfiguration configuration, IWebHostEnvironment env)
        {
            services.AddScoped<IUserRepository, UserRepository>();
            services.AddScoped<IRoleRepository, RoleRepository>();
            services.AddScoped<IPieceRepository, PieceRepository>();
            services.AddScoped<IMenuRepository, MenuRepository>();
            services.AddScoped<IAreaRepository, AreaRepository>();
            services.AddScoped<IFoodRepository, FoodRepository>();
            services.AddScoped<ITicketRepository, TicketRepository>();
            services.AddScoped<ICageRepository, CageRepository>();
            services.AddScoped<IArticleRepository, ArticleRepository>();
            services.AddScoped<IAnimalProfileRepository, AnimalProfileRepository>();
            services.AddScoped<IProfileHistoryRepository, ProfileHistoryRepository>();
            services.AddScoped<IMealRepository, MealRepository>();
            services.AddScoped<IRequestRepository, RequestRepository>();
            services.AddScoped<IOrderRepository, OrderRepository>();
            services.AddScoped<IOrderDetailRepository, OrderDetailRepository>();


            
            services.AddScoped<IUserService, UserService>();
            services.AddScoped<IRoleService, RoleService>();
            services.AddScoped<IPieceService, PieceService>();
            services.AddScoped<IMenuService, MenuService>();
            services.AddScoped<IAreaService, AreaService>();
            services.AddScoped<IFoodService, FoodService>();
            services.AddScoped<ITicketService, TicketService>();
            services.AddScoped<ICageService, CageService>();
            services.AddScoped<IArticleService, ArticleService>();
            services.AddScoped<IAnimalProfileService, AnimalProfileService>();
            services.AddScoped<IProfileHistoryService, ProfileHistoryService>();
            services.AddScoped<IMealService, MealService>();
            services.AddScoped<IRequestService, RequestService>();
            services.AddScoped<IOrderService, OrderService>();



            services.AddScoped<IUnitOfWork, UnitOfWork>();
            services.AddSingleton<ICurrentTime, CurrentTime>();


            services.AddDbContext<AppDbContext>(options =>
                    options.UseSqlServer(GetConnection(configuration, env),
                        builder => builder.MigrationsAssembly(typeof(AppDbContext).Assembly.FullName)));

            services.AddAutoMapper(typeof(MapperConfigurationsProfile).Assembly);



            return services;
        }
        private static string GetConnection(IConfiguration configuration, IWebHostEnvironment env)
        {
            return configuration[$"ConnectionStrings:{env.EnvironmentName}"]
                ?? throw new Exception($"ConnectionStrings:{env.EnvironmentName} not found");
        }

    }


}
