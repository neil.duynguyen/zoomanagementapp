﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Infrastructures.Migrations
{
    /// <inheritdoc />
    public partial class UpdateNameTable : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ProfileHistorie_AnimalProfile_AnimalProfileId",
                table: "ProfileHistorie");

            migrationBuilder.DropPrimaryKey(
                name: "PK_ProfileHistorie",
                table: "ProfileHistorie");

            migrationBuilder.RenameTable(
                name: "ProfileHistorie",
                newName: "ProfileHistories");

            migrationBuilder.RenameIndex(
                name: "IX_ProfileHistorie_AnimalProfileId",
                table: "ProfileHistories",
                newName: "IX_ProfileHistories_AnimalProfileId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_ProfileHistories",
                table: "ProfileHistories",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_ProfileHistories_AnimalProfile_AnimalProfileId",
                table: "ProfileHistories",
                column: "AnimalProfileId",
                principalTable: "AnimalProfile",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ProfileHistories_AnimalProfile_AnimalProfileId",
                table: "ProfileHistories");

            migrationBuilder.DropPrimaryKey(
                name: "PK_ProfileHistories",
                table: "ProfileHistories");

            migrationBuilder.RenameTable(
                name: "ProfileHistories",
                newName: "ProfileHistorie");

            migrationBuilder.RenameIndex(
                name: "IX_ProfileHistories_AnimalProfileId",
                table: "ProfileHistorie",
                newName: "IX_ProfileHistorie_AnimalProfileId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_ProfileHistorie",
                table: "ProfileHistorie",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_ProfileHistorie_AnimalProfile_AnimalProfileId",
                table: "ProfileHistorie",
                column: "AnimalProfileId",
                principalTable: "AnimalProfile",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
