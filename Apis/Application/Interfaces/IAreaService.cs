﻿using Application.ViewModels.AreaViewModels;
using Domain.Entities;

namespace Application.Interfaces
{
    public interface IAreaService
    {
        Task<List<AreaViewModelDetail>> GetArea();
        Task<AreaViewModelDetail> GetAreaById(Guid id);
        Task<AreaViewModel> CreateArea(CreateAreaViewModels name);
        Task<AreaViewModel> UpdateArea(UpdateAreaViewModel model);
        Task<bool> RemoveArea(Guid Id);
    }
}
