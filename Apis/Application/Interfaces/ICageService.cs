﻿using Application.ViewModels.CageViewModels;
using Domain.Entities;

namespace Application.Interfaces
{
    public interface ICageService
    {
        List<CageViewModel> GetCage();
        Task<CageViewModel> GetCageById(Guid id);
        Task<List<CageViewModel>> GetCageByAreaID(Guid id);
        Task<CageViewModel> CreateCage(CreateCageViewModel model);
        Task<CageViewModel> UpdateCage(UpdateCageViewModel model);
        Task<bool> RemoveCage(Guid Id);
    }
}
