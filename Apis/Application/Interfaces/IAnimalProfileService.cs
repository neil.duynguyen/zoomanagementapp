﻿using Application.ViewModels.AnimalProfileViewModels;

namespace Application.Interfaces
{
    public interface IAnimalProfileService
    {
        Task<List<AnimalProfileViewModelDetail>> GetAnimalProfile();
        Task<AnimalProfileViewModelDetail> GetAnimalProfileById(Guid id);
        Task<AnimalProfileViewModel> CreateAnimalProfile(CreateAnimalProfileViewModel model);
        Task<AnimalProfileViewModel> UpdateAnimalProfile(UpdateAnimalProfileViewModel model);
        Task<bool> RemoveAnimalProfile(Guid Id);
        Task<AnimalProfileViewModel> ChangeCage(ChangeCageViewModel model);
        Task<List<AnimalProfileViewModelDetail>> SearchAnimalProfile(SearchAnimalProfileViewModel model);
    }
}
