﻿using Application.ViewModels.RoleViewModels;
using Application.ViewModels.TicketViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Interfaces
{
    public interface IRoleService
    {
        Task<List<RoleViewModel>> GetRole();
        Task<bool> CreateRole(string roleView);
    }
}
