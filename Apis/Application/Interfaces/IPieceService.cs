﻿using Application.ViewModels.PieceViewModels;
using Domain.Entities;

namespace Application.Interfaces
{
    public interface IPieceService
    {
        List<PieceViewModel> GetPiece();
        Task<PieceViewModel> GetPieceById(Guid id);
        Task<PieceViewModel> CreatePiece(CreatePieceViewModel createPieceView);
        Task<PieceViewModel> UpdatePiece(UpdatePieceViewModel updatePieceView);
        Task<bool> RemovePiece(Guid Id);
    }
}
