﻿using Application.ViewModels.RequestViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Interfaces
{
    public interface IRequestService
    {
        List<RequestViewModel> GetRequest();
        Task<RequestViewModel> GetRequestById(Guid id);
        Task<RequestViewModel> CreateRequest(CreateRequestViewModel model);
        //Task<RequestViewModel> UpdateRequest(CreateRequestViewModel model);
        Task<RequestViewModel> UpdateStatusRequest(UpdateStatusRequestViewModel model);
        Task<bool> RemoveRequest(Guid Id);
        Task<List<RequestViewModel>> GetRequestByCreateDate(string createDate);
    }
}
