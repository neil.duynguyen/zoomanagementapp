﻿using Application.ViewModels.MealViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Interfaces
{
    public interface IMealService
    {
        Task<List<MealViewModel>> GetMealByAnimalId(Guid animalID);
        Task<List<MealViewModel>> GetMealByIdUser();
        Task<List<MealViewModel>> CreateMeal(CreateMealViewModel createMeal);
        Task<StatisticsFoodViewModel> StatisticsFood(StatisticsFoodRequest model);
       // Task<MealViewModel> UpdateMeal(UpdateMealViewModel MealView);
    }
}
