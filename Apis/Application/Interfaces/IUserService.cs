﻿using Application.ViewModels.AuthViewModel;
using Application.ViewModels.UserViewModels;

namespace Application.Interfaces
{
    public interface IUserService
    {
        public Task RegisterAsync(RegisterModel userObject);
        public Task RegisterEmployeeAsync(RegisterEmployeeModel userObject);
        public Task<LoginViewModel> LoginAsync(UserLoginViewModel userObject);
        Task<List<UserViewModel>> GetUser();
        Task<UserViewModel> GetUserById(Guid id);
        Task<UserViewModel> UpdateUser(UpdateUserViewModel updateUserView);
        Task<UserViewModel> UpdateRoleUser(UpdateUserRoleViewModel updateUserRoleViewModel);
        Task<bool> RemoveUser(Guid Id);
        Task<UserViewModel> ChangePassword(ChangePasswordModel model);
        Task<UserViewModel> ResetPassword(ResetPasswordModel model);
        Task<bool> ForgotPassword(ForgotPasswordModel model);
        Task<bool> SendEmail(SendEmailModel model);
    }
}
