﻿using Application.ViewModels.AnimalProfileViewModels;
using Application.ViewModels.MenuViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Interfaces
{
    public interface IMenuService
    {
        Task<List<MenuViewModel>> GetMenu();
        Task<List<MenuViewModel>> GetMenuByIdUser();
        Task<List<MenuViewModel>> GetMenuByIdAnimal(Guid AnimalId);
        Task<List<MenuViewModel>> GetAllMenuByIdAnimal(Guid AnimalId);

        Task<MenuViewModel> CreateMenu(CreateMenuViewModel menuView);
        Task<MenuViewModel> UpdateMenu(UpdateMenuViewModel menuView);
        Task<bool> RemoveMenu(Guid Id);
        Task<List<MenuViewModelDetail>> SearchMenu(SearchMenuModel model);

    }
}
