﻿using Application.ViewModels.ArticleViewModels;
using Application.ViewModels.ArticleViewModels;
using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Interfaces
{
    public interface IArticleService
    {
        List<ArticleViewModelDetail> GetArticle();
        Task<ArticleViewModelDetail> GetArticleById(Guid id);
        List<ArticleViewModelDetail> GetArticleByTitle(string title);
        Task<ArticleViewModel> CreateArticle(CreateArticleViewModel createArticle);
        Task<ArticleViewModel> UpdateArticle(UpdateArticleViewModel articleViewModel);
        Task<ArticleViewModel> UpdateArticleStatus(UpdateStatusArticleViewModel model);
        Task<bool> RemoveArticle(Guid Id);
        Task<List<ArticleViewModelDetail>> SearchArticles(SearchArticleViewModel model);
    }
}
