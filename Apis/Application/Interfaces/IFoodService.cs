﻿using Application.ViewModels.FoodViewModels;
using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Interfaces
{
    public interface IFoodService
    {
        List<FoodViewModel> GetFood();
        Task<FoodViewModel> GetFoodById(Guid id);
        Task<FoodViewModel> CreateFood(CreateFoodViewModel foodView);
        Task<FoodViewModel> UpdateFood(UpdateFoodViewModel foodView);
        Task<bool> RemoveFood(Guid Id);
    }
}
