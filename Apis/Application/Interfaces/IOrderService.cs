﻿using Application.ViewModels.OrderViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Interfaces
{
    public interface IOrderService
    {
        public Task<OrderViewModel> CreateOrder(CreateOrderDetailViewModel createOrder);
        public Task<object> CreatePayment(string billId);
        public Task UpdateStatusOrder(UpdateOrderViewModel updateOrderView);
    }
}
