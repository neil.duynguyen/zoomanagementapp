﻿using Application.ViewModels.ProfileHistoryViewModels;
using Application.ViewModels.ProfileHistoryViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Interfaces
{
    public interface IProfileHistoryService
    {
        Task<List<ProfileHistoryViewModel>> GetProfileHistory();
        Task<ProfileHistoryViewModel> GetProfileHistoryById(Guid id);
        Task<ProfileHistoryViewModel> CreateProfileHistory(Guid animalProfileId);
        Task<ProfileHistoryViewModel> UpdateProfileHistory(Guid Id, Guid animalProfileId);
        Task<bool> RemoveProfileHistory(Guid Id);
    }
}
