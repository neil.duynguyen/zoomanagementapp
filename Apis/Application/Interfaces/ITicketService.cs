﻿using Application.ViewModels.FoodViewModels;
using Application.ViewModels.TicketViewModels;
using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Interfaces
{
    public interface ITicketService
    {
        List<TicketViewModel> GetTicket();
        Task<TicketViewModel> GetTicketById(Guid id);
        Task<TicketViewModel> CreateTicket(CreateTicketViewModel foodView);
        Task<TicketViewModel> UpdateTicket(UpdateTicketModel model);
        Task<bool> RemoveTicket(Guid Id);
    }
}
