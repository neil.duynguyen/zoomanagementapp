﻿namespace Application.Interfaces
{
    public interface ICurrentTime
    {
        public DateTime GetCurrentTime();
        public double? Timestamp(DateTime dateTime);
    }
}
