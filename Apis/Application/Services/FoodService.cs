﻿using Application.Interfaces;
using Application.ViewModels.FoodViewModels;
using AutoMapper;
using Domain.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Services
{
    public class FoodService : IFoodService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly ICurrentTime _currentTime;
        private readonly IMapper _mapper;

        public FoodService(IUnitOfWork unitOfWork, ICurrentTime currentTime, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _currentTime = currentTime;
            _mapper = mapper;
        }

        public List<FoodViewModel> GetFood()
        {
            var mapper = _mapper.Map<List<FoodViewModel>>(_unitOfWork.FoodRepository.GetAllAsync().Result.Where(x => x.IsDeleted == false));

            return mapper;
        }

        public async Task<FoodViewModel> GetFoodById(Guid id)
        {
            var food = await _unitOfWork.FoodRepository.GetByIdAsync(id);

            if (food is null || food.IsDeleted)
                throw new Exception("Không tìm thấy thực phẩm");

            var mapper = _mapper.Map<FoodViewModel>(food);

            return mapper;
        }

        public async Task<FoodViewModel> CreateFood(CreateFoodViewModel foodView)
        {
            var checkName = _unitOfWork.FoodRepository.GetAllAsync().Result.Where(x => x.Name.ToLower().Equals(foodView.Name.ToLower()) && x.IsDeleted == false).ToList();

            if (checkName.Count != 0)
                throw new Exception("Tên thực phẩm không được trùng.");

            var mapper = _mapper.Map<Food>(foodView);

            await _unitOfWork.FoodRepository.AddAsync(mapper);

            return await _unitOfWork.SaveChangeAsync() > 0 ? _mapper.Map<FoodViewModel>(mapper) : throw new Exception("Tạo thực phẩm thất bại");
        }

        public async Task<bool> RemoveFood(Guid Id)
        {
            var findFood = await _unitOfWork.FoodRepository.GetByIdAsync(Id);


            if (findFood is not null && findFood.IsDeleted == false)
            {
                _unitOfWork.FoodRepository.Remove(findFood);

                return await _unitOfWork.SaveChangeAsync() > 0 ? true : false;
            }

            throw new Exception("Không tìm thấy thực phẩm.");

        }

        public async Task<FoodViewModel> UpdateFood(UpdateFoodViewModel foodView)
        {

            var findFood = await _unitOfWork.FoodRepository.GetByIdAsync(foodView.Id);

            if (findFood is not null && findFood.IsDeleted == false)
            {
                _unitOfWork.FoodRepository.Update(_mapper.Map(foodView, findFood));

                return await _unitOfWork.SaveChangeAsync() > 0 ? _mapper.Map<FoodViewModel>(findFood) : throw new Exception("Cập nhật thực phẩm thất bại");
            }

            throw new Exception("Không tìm thấy thực phẩm.");
        }
    }
}
