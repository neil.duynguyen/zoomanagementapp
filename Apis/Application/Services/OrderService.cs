﻿using Application.Interfaces;
using Application.ViewModels.OrderViewModels;
using AutoMapper;
using Domain.Entities;
using Domain.Enums;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using Microsoft.VisualBasic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;
using System.Web;
using static System.Runtime.CompilerServices.RuntimeHelpers;
using static System.Runtime.InteropServices.JavaScript.JSType;

namespace Application.Services
{
    public class OrderService : IOrderService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly ICurrentTime _currentTime;
        private readonly IClaimsService _claimsService;
        private readonly IMapper _mapper;
        private readonly IConfiguration _configuration;

        public OrderService(IUnitOfWork unitOfWork, ICurrentTime currentTime, IClaimsService claimsService, IMapper mapper, IConfiguration configuration)
        {
            _unitOfWork = unitOfWork;
            _currentTime = currentTime;
            _claimsService = claimsService;
            _mapper = mapper;
            _configuration = configuration;
        }


        public string GenerateVnPayPaymentUrl(string orderId, double total)
        {
            try
            {
                const string VnPayApiUrl = "https://sandbox.vnpayment.vn/paymentv2/vpcpay.html";
                const string VnPayTmnCode = "5UNK3BH1";
                const string VnPayHashSecret = "VXOKYGMKDDSKCGCLMIOKARSVNRYJWCKV";
                string version = "2.1.0";
                string command = "pay";
                string locale = "vn";
                string currCode = "VND";
                string orderInfo = "Order info";
                string returnUrl = "google.com";
                //string bankCode = "VNBANK";
                string createDate = FormatDate(_currentTime.GetCurrentTime());
                string vnp_Amount = (total * 100).ToString();

                var vnpParams = new Dictionary<string, string>()
            {
                { "vnp_Version", version },
                    { "vnp_Command", command },
                    { "vnp_TmnCode", VnPayTmnCode },
                    { "vnp_Locale", locale },
                    { "vnp_CurrCode", currCode },
                    { "vnp_TxnRef", orderId },
                    { "vnp_OrderInfo", orderInfo },
                    { "vnp_OrderType", "other" },
                    { "vnp_Amount", (total * 100).ToString() },
                    { "vnp_ReturnUrl", returnUrl },
                    { "vnp_IpAddr", "192.168.1.3" },
                    { "vnp_CreateDate", createDate }
                    //{ "vnp_BankCode", bankCode }
            };
                vnpParams = SortDictionary(vnpParams);
                string signData = string.Join("&", vnpParams.Select(kvp => $"{kvp.Key}={HttpUtility.UrlEncode(kvp.Value)}"));
                Console.WriteLine($"signData: {signData}");

                using (HMACSHA512 hmac = new HMACSHA512(Encoding.UTF8.GetBytes(VnPayHashSecret)))
                {
                    byte[] hashBytes = hmac.ComputeHash(Encoding.UTF8.GetBytes(signData));
                    string signed = BitConverter.ToString(hashBytes).Replace("-", "").ToLower();
                    vnpParams["vnp_SecureHash"] = signed;
                }

                string fullUrl = $"{VnPayApiUrl}?{string.Join("&", vnpParams.Select(kvp => $"{kvp.Key}={HttpUtility.UrlEncode(kvp.Value)}"))}";
                Uri url = new Uri(fullUrl);
                return url.AbsoluteUri;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                throw new Exception("Generate URL VNPay payment fail.");
            }
        }

        private Dictionary<string, string> SortDictionary(Dictionary<string, string> dict)
        {
            return dict.OrderBy(x => x.Key).ToDictionary(x => x.Key, x => x.Value);
        }

        private string FormatDate(DateTime date)
        {
            return date.ToString("yyyyMMddHHmmss");
        }

        public async Task<OrderViewModel> CreateOrder(CreateOrderDetailViewModel createOrder)
        {
            Order order = new Order()
            {
                Id = Guid.NewGuid(),
                UserId = _claimsService.GetCurrentUserId.ToString().Equals("00000000-0000-0000-0000-000000000000") ? new Guid("3F4AAEF9-FF3C-41EA-DAC2-08DBC4DD143F") : new Guid(_claimsService.GetCurrentUserId.ToString()),
                OrderStatus = OrderStatus.Unpaid,
                TotalAmount = 0,
                OrderNumber = "Order" + (_unitOfWork.OrderRepository.GetAllAsync().Result.Count + 1)
            };

            await _unitOfWork.OrderRepository.AddAsync(order);

            var save = await _unitOfWork.SaveChangeAsync();

            if (save > 0)
            {
                double total = 0;
                foreach (var item in createOrder.orderDetailViewModels)
                {
                    OrderDetail orderDetail = new OrderDetail() 
                    { 
                        OrderId = order.Id,
                        TicketId = _unitOfWork.TicketRepository.GetAllAsync().Result.FirstOrDefault(x => x.TicketType.Equals(item.TicketType)).Id,
                        Quantity = item.Quantity,
                        TicketPrice = _unitOfWork.TicketRepository.GetAllAsync().Result.FirstOrDefault(x => x.TicketType.Equals(item.TicketType)).Price,
                        Total = item.Quantity * _unitOfWork.TicketRepository.GetAllAsync().Result.FirstOrDefault(x => x.TicketType.Equals(item.TicketType)).Price,
                        CreatedBy = _claimsService.GetCurrentUserId
                    };
                    await _unitOfWork.OrderDetailRepository.AddAsync(orderDetail);
                    await _unitOfWork.SaveChangeAsync();
                    total += orderDetail.Total;
                }             
                order.TotalAmount = total;
                await _unitOfWork.SaveChangeAsync();
                return _mapper.Map<OrderViewModel>(order);

            }
            else
            {
                throw new Exception("Tạo order thất bại.");
            }
        }

        public async Task UpdateStatusOrder(UpdateOrderViewModel updateOrderView)
        {
            var order = await _unitOfWork.OrderRepository.GetByIdAsync(updateOrderView.Id);
            if (order == null) { throw new Exception("Lỗi cập nhật trạng thái đơn hàng."); }

            order.OrderStatus = OrderStatus.Paid;
            order.ModificationDate = _currentTime.GetCurrentTime();
            _unitOfWork.SaveChangeAsync();
        }

        public async Task<object> CreatePayment(string billId)
        {
            try
            {
                var bill = await _unitOfWork.OrderRepository.GetByIdAsync(new Guid(billId));
                /*if (bill.Status == "new")
                {
                    return new InternalError("Bill has not been approved");
                }
                if (bill.Status == "success")
                {
                    return new InternalError("Bill has been paid");
                }*/
                var url = GenerateVnPayPaymentUrl(bill.Id.ToString(), bill.TotalAmount);
                return new
                {
                    StatusCode = 200,
                    Url = url
                };
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                /*if (ex is NotFoundError)
                {
                    return ex;
                }*/
                return new Exception(ex.Message);
            }
        }

        /*public async Task ProcessIpn(IPNViewModel pNViewModel)
        {
            const string VnPayHashSecret = "VXOKYGMKDDSKCGCLMIOKARSVNRYJWCKV";

            Dictionary<string, string> vnpParams = new Dictionary<string, string>
            {
                { "vnp_TmnCode", pNViewModel.tmnCode },
                { "vnp_Amount", pNViewModel.ammount.ToString() },
                { "vnp_BankCode", pNViewModel.bankCode },
                { "vnp_OrderInfo", pNViewModel.orderInfo },
                { "vnp_TransactionNo", pNViewModel.transactionNo },
                { "vnp_ResponseCode", pNViewModel.responseCode },
                { "vnp_TransactionStatus", pNViewModel.transactionStatus },
                { "vnp_TxnRef", pNViewModel.txnRef },
                { "vnp_BankTranNo", pNViewModel.bankTranNo },
                { "vnp_CardType", pNViewModel.cardType },
                { "vnp_PayDate", pNViewModel.payDate },
                { "vnp_SecureHash", pNViewModel.secureHash }
            };

            //string status = VNPay_STATUS.TryGetValue(responseCode, out string value) ? value : ORDER_STATUS_ERROR;


        }*/


    }
}
