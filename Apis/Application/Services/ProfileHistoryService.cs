﻿using Application.Interfaces;
using Application.Repositories;
using Application.ViewModels.ProfileHistoryViewModels;
using AutoMapper;
using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Services
{
    public class ProfileHistoryService : IProfileHistoryService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly ICurrentTime _currentTime;
        private readonly IClaimsService _claimsService;
        private readonly IMapper _mapper;
        private readonly IProfileHistoryRepository _profileHistoryRepository;
        private readonly IUserRepository _userRepository;

        public ProfileHistoryService(IUnitOfWork unitOfWork, ICurrentTime currentTime, IClaimsService claimsService, IMapper mapper, IProfileHistoryRepository profileHistoryRepository, IUserRepository userRepository)
        {
            _unitOfWork = unitOfWork;
            _currentTime = currentTime;
            _claimsService = claimsService;
            _mapper = mapper;
            _profileHistoryRepository = profileHistoryRepository;
            _userRepository = userRepository;
        }

        public async Task<List<ProfileHistoryViewModel>> GetProfileHistory()
        {
            var mapper = _unitOfWork.ProfileHistoryRepository.GetAllAsync().Result.Where(x => x.IsDeleted == false)
                .OrderByDescending(x => x.CreationDate).ToList();

            List<ProfileHistoryViewModel> List = new List<ProfileHistoryViewModel>();
            foreach (var item in mapper)
            {
               var history = _mapper.Map<ProfileHistoryViewModel>(item);
                dynamic createById = item.CreatedBy;
                history.CreatedBy =  _userRepository.GetByIdAsync(createById).Result.FullName;

                List.Add(history);
            }
            return List;
        }

        public async Task<ProfileHistoryViewModel> GetProfileHistoryById(Guid id)
        {
            var ProfileHistory = await _unitOfWork.ProfileHistoryRepository.GetByIdAsync(id);
            if (ProfileHistory is null || ProfileHistory.IsDeleted)
            {
                throw new Exception("Không tìm thấy lịch sử hồ sơ");
            }
            var viewModel = _mapper.Map<ProfileHistoryViewModel>(ProfileHistory);
            //viewModel.CreatedBy = ProfileHistory.AnimalProfile.User.FullName;
            dynamic createById = viewModel.CreatedBy;
            viewModel.CreatedBy = _userRepository.GetByIdAsync(createById).Result.FullName;

            return viewModel;
        }

        public async Task<ProfileHistoryViewModel> CreateProfileHistory(Guid animalProfileId)
        {
            /*var checkName = _unitOfWork.ProfileHistoryRepository.GetAllAsync().Result.Where(x => x.Name.ToLower().Equals(name.ToLower()) && x.IsDeleted == false).ToList();
            if (checkName.Count != 0)
                throw new Exception("Tên lịch sử hồ sơ không được trùng");*/
            CreateProfileHistoryViewModel viewModel = new() { AnimalProfileId = animalProfileId };
            var mapper = _mapper.Map<ProfileHistory>(viewModel);

            await _unitOfWork.ProfileHistoryRepository.AddAsync(mapper);

            return await _unitOfWork.SaveChangeAsync() > 0 ? _mapper.Map<ProfileHistoryViewModel>(mapper) : throw new Exception("Tạo lịch sử hồ sơ thất bại");

        }

        public async Task<ProfileHistoryViewModel> UpdateProfileHistory(Guid Id, Guid animalProfileId)
        {
            var findProfileHistory = await _unitOfWork.ProfileHistoryRepository.GetByIdAsync(Id);

            if (findProfileHistory is not null && findProfileHistory.IsDeleted == false)
            {
                /*if (!name.Equals(findProfileHistory.Name))
                {*/
                    findProfileHistory.AnimalProfileId = animalProfileId;
                    _unitOfWork.ProfileHistoryRepository.Update(findProfileHistory);

                    return await _unitOfWork.SaveChangeAsync() > 0 ? _mapper.Map<ProfileHistoryViewModel>(findProfileHistory) : throw new Exception("Cập nhật lịch sử hồ sơ thất bại");
                /*}
                else
                {
                    throw new Exception("Cập nhật lịch sử hồ sơ không được trùng");
                }*/

            }
            throw new Exception("Không tìm thấy lịch sử hồ sơ.");
        }

        public async Task<bool> RemoveProfileHistory(Guid Id)
        {
            var findProfileHistory = _profileHistoryRepository.getProfileHistoryByIdWithRelation(Id);
            if (findProfileHistory is not null && findProfileHistory.IsDeleted == false)
            {
                /*if (findProfileHistory.AnimalProfile.Count() == 0)
                {*/
                    _unitOfWork.ProfileHistoryRepository.Remove(findProfileHistory);

                    return await _unitOfWork.SaveChangeAsync() > 0 ? true : false;
                /*}
                else
                {
                    throw new Exception("Xóa thất bại, hiện lịch sử hồ sơ này vẫn còn thú tồn tại");
                }*/
            }

            throw new Exception("Không tìm thấy lịch sử hồ sơ.");

        }
    }
}
