﻿using Application.Interfaces;
using Application.Repositories;
using Application.ViewModels.RequestViewModels;
using AutoMapper;
using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Services
{
    public class RequestService : IRequestService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly ICurrentTime _currentTime;
        private readonly IClaimsService _claimsService;
        private readonly IMapper _mapper;
        private readonly IRequestRepository _RequestRepository;

        public RequestService(IUnitOfWork unitOfWork, ICurrentTime currentTime, IClaimsService claimsService, IMapper mapper, IRequestRepository RequestRepository)
        {
            _unitOfWork = unitOfWork;
            _currentTime = currentTime;
            _claimsService = claimsService;
            _mapper = mapper;
            _RequestRepository = RequestRepository;
        }

        public List<RequestViewModel> GetRequest()
        {
            var mapper = _mapper.Map<List<RequestViewModel>>(_unitOfWork.RequestRepository.GetAllAsync().Result.Where(x => x.IsDeleted == false)
                .OrderByDescending(x => x.CreationDate));

            return mapper;
        }

        public async Task<RequestViewModel> GetRequestById(Guid id)
        {
            var Request = await _unitOfWork.RequestRepository.GetByIdAsync(id);
            if (Request is null || Request.IsDeleted)
            {
                throw new Exception("Không tìm thấy yêu cầu");
            }
            var viewModel = _mapper.Map<RequestViewModel>(Request);
            return viewModel;
        }

        public async Task<RequestViewModel> CreateRequest(CreateRequestViewModel model)
        {
            /*var checkName = _unitOfWork.RequestRepository.GetAllAsync().Result.Where(x => x.Name.ToLower().Equals(name.ToLower()) && x.IsDeleted == false).ToList();
            if (checkName.Count() != 0)
                throw new Exception("Khu vực không được trùng");

            CreateRequestViewModel RequestView = new()
            {
                Name = name,
                UserId = _claimsService.GetCurrentUserId
            };*/

            var mapper = _mapper.Map<Request>(model);
            mapper.UserId = _claimsService.GetCurrentUserId;
            mapper.RequestStatus = Domain.Enums.RequestStatus.Request;
            mapper.CreationDate = DateTime.UtcNow;

            await _unitOfWork.RequestRepository.AddAsync(mapper);

            return await _unitOfWork.SaveChangeAsync() > 0 ? _mapper.Map<RequestViewModel>(mapper) : throw new Exception("Tạo yêu cầu thất bại");

        }

        /*public async Task<RequestViewModel> UpdateRequest(CreateRequestViewModel model)
        {
            var findRequest = await _unitOfWork.RequestRepository.GetByIdAsync(model.Id);

            if (findRequest is not null && findRequest.IsDeleted == false)
            {
                *//*if (!name.Equals(findRequest.Name))
                {*//*
                var mapper = _mapper.Map(model, findRequest);
                _unitOfWork.RequestRepository.Update(mapper);

                return await _unitOfWork.SaveChangeAsync() > 0 ? _mapper.Map<RequestViewModel>(mapper) : throw new Exception("Cập nhật yêu cầu thất bại");
                *//*}
                else
                {
                    throw new Exception("Cập nhật khu vực không được trùng");
                }*//*

            }
            throw new Exception("Không tìm thấy yêu cầu.");
        }*/

        public async Task<RequestViewModel> UpdateStatusRequest(UpdateStatusRequestViewModel model)
        {
            var findRequest = await _unitOfWork.RequestRepository.GetByIdAsync(model.Id);

            if (findRequest is not null && findRequest.IsDeleted == false)
            {
                /*if (model.Status.ToLower().Equals(findRequest.RequestStatus.ToString().ToLower()))
                {*/
                findRequest.RequestStatus = model.Status.ToLower() switch
                {
                    "request" => throw new Exception("Cập nhật trạng thái thất bại, yêu cầu của bạn đã được xử lí"),
                    "approved" => Domain.Enums.RequestStatus.Approved,
                    "rejected" => Domain.Enums.RequestStatus.Rejected,
                    "canceled" => Domain.Enums.RequestStatus.Canceled,
                    _ => throw new Exception("Trạng thái không nằm trong quy định"),
                };

                //var mapper = _mapper.Map(model, findRequest);
                _unitOfWork.RequestRepository.Update(findRequest);

                return await _unitOfWork.SaveChangeAsync() > 0 ? _mapper.Map<RequestViewModel>(findRequest) : throw new Exception("Cập nhật yêu cầu thất bại");
                /*}
                else
                {
                    throw new Exception("Cập nhật trạng thái thất bại, yêu cầu của bạn đã được xử lí");
                }*/

            }
            throw new Exception("Không tìm thấy yêu cầu.");
        }

        public async Task<bool> RemoveRequest(Guid Id)
        {
            var findRequest = await _RequestRepository.GetByIdAsync(Id);
            if (findRequest is not null && findRequest.IsDeleted == false)
            {
                /*if (findRequest.ProfileHistories is null)
                {*/
                _unitOfWork.RequestRepository.Remove(findRequest);

                return await _unitOfWork.SaveChangeAsync() > 0 ? true : false;
                /*}
                else
                {
                    throw new Exception("Xóa thất bại, hiện yêu cầu này vẫn còn tồn tại bữa ăn hoặc lịch sử hồ sơ");
                }*/
            }

            throw new Exception("Không tìm thấy yêu cầu.");

        }

        public async Task<List<RequestViewModel>> GetRequestByCreateDate(string createDate)
        {
            var findRequest = await _unitOfWork.RequestRepository.GetRequestByCreateDate(createDate);
                    return _mapper.Map<List<RequestViewModel>>(findRequest);
        }

    }
}
