﻿using Application.Interfaces;
using Application.ViewModels.MealViewModels;
using Application.ViewModels.MenuViewModels;
using AutoMapper;
using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Services
{
    public class MealService : IMealService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly ICurrentTime _currentTime;
        private readonly IClaimsService _claimsService;
        private readonly IMapper _mapper;

        public MealService(IUnitOfWork unitOfWork, ICurrentTime currentTime, IClaimsService claimsService, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _currentTime = currentTime;
            _claimsService = claimsService;
            _mapper = mapper;
        }

        public async Task<List<MealViewModel>> CreateMeal(CreateMealViewModel createMeal)
        {
            foreach (var item in createMeal.createMeals)
            {
                if (item.ActualQuantily <= 0) throw new Exception("Số lượng thức ăn thực tế phải lớn hơn 0.");
            }
            
            var mapper = _mapper.Map<List<Meal>>(createMeal.createMeals);

            foreach (var item in mapper)
            {
                item.Date = _currentTime.GetCurrentTime();
            }

            await _unitOfWork.MealRepository.AddRangeAsync(mapper);

            return await _unitOfWork.SaveChangeAsync() > 0 ? _mapper.Map<List<MealViewModel>>(mapper) : throw new Exception("Tạo bữa ăn thất bại");
        }

        public async Task<List<MealViewModel>> GetMealByAnimalId(Guid animalID)
        {
            var meal = _unitOfWork.MealRepository.GetAllAsync().Result.Where(x => x.Menu.AnimalProfileId == animalID);

            List<MealViewModel> listMeal = new List<MealViewModel>();

            foreach (var item in meal) {

                var menu = await _unitOfWork.MenuRepository.GetByIdAsync(item.MenuId);
                var mapper = _mapper.Map<MealViewModel>(item);
                mapper.MenuName = menu.Name;
                mapper.NameAnimal = menu.AnimalProfile.Name;
                listMeal.Add(mapper);
            }    

            return listMeal;
        }

        public async Task<List<MealViewModel>> GetMealByIdUser()
        {
            var meal = _unitOfWork.MealRepository.GetAllAsync().Result.Where(x => x.CreatedBy == _claimsService.GetCurrentUserId).ToList();

            List<MealViewModel> listMeal = new List<MealViewModel>();

            foreach (var item in meal)
            {

                var menu = await _unitOfWork.MenuRepository.GetByIdAsync(item.MenuId);
                var mapper = _mapper.Map<MealViewModel>(item);
                mapper.MenuName = menu.Name;
                mapper.NameAnimal = menu.AnimalProfile.Name;
                listMeal.Add(mapper);
            }

            return listMeal;
        }

        public async Task<StatisticsFoodViewModel> StatisticsFood(StatisticsFoodRequest model)
        {
            var getMenu = _unitOfWork.MenuRepository.GetAllAsync().Result.Where(x => x.AnimalProfile.CageId == model.CageId).ToList();

            List<Meal> list = new List<Meal>();
            foreach (var item in getMenu)
            {
                var meal = _unitOfWork.MealRepository.GetAllAsync().Result.Where(x => x.MenuId == item.Id  && x.Date >= model.StartDate && x.Date <= model.EndDate).ToList();

                if (meal.Count != 0)
                {
                    foreach (var item3 in meal)
                    {
                        list.Add(item3);
                    }
                }
            }

            List<FoodView> foodView = new List<FoodView>();

            foreach (var item in list)
            {
                if (foodView.Count == 0)
                {
                    FoodView food = new FoodView()
                    {
                        Name = item.MenuId.ToString(),
                        ActualAmountOfConsumption = item.ActualQuantily,
                        ExpectedAmountOfConsumption = _unitOfWork.MenuRepository.GetByIdAsync(item.MenuId).Result.ExpectedQuantity
                    };
                    foodView.Add(food);
                }
                else
                {
                    bool check = false;
                    foreach (var item2 in foodView)
                    {
                        if (item.MenuId.ToString().Equals(item2.Name))
                        {
                            item2.ActualAmountOfConsumption += item.ActualQuantily;
                            item2.ExpectedAmountOfConsumption += _unitOfWork.MenuRepository.GetByIdAsync(item.MenuId).Result.ExpectedQuantity;
                            check = true;
                            break;
                        }
                    }

                    if (!check)
                    {
                        FoodView food = new FoodView()
                        {
                            Name = item.MenuId.ToString(),
                            ActualAmountOfConsumption = item.ActualQuantily,
                            ExpectedAmountOfConsumption = _unitOfWork.MenuRepository.GetByIdAsync(item.MenuId).Result.ExpectedQuantity
                        };
                        foodView.Add(food);
                    }
                }
                
            }

            foreach (var item in foodView)
            {
                item.Name = _unitOfWork.MenuRepository.GetByIdAsync(new Guid(item.Name)).Result.Food.Name;
            }

            List<FoodView> foodView1 = new List<FoodView>();

            foreach (var group in foodView.GroupBy(x => x.Name))
            {
                double totalActual = group.Sum(x => x.ActualAmountOfConsumption);
                double totalExpected = group.Sum(x => x.ExpectedAmountOfConsumption);

                foodView1.Add(new FoodView()
                {
                    Name = group.Key,
                    ActualAmountOfConsumption = totalActual,
                    ExpectedAmountOfConsumption = totalExpected,
                });
            }

            return new StatisticsFoodViewModel { 
                CageId = model.CageId,
                Food = foodView1
            };

        }
    }
}
