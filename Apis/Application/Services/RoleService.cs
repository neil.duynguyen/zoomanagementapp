﻿using Application.Interfaces;
using Application.ViewModels.RoleViewModels;
using AutoMapper;
using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Services
{
    public class RoleService : IRoleService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly ICurrentTime _currentTime;
        private readonly IClaimsService _claimsService;
        private readonly IMapper _mapper;

        public RoleService(IUnitOfWork unitOfWork, ICurrentTime currentTime, IClaimsService claimsService, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _currentTime = currentTime;
            _claimsService = claimsService;
            _mapper = mapper;
        }

        public async Task<bool> CreateRole(string name)
        {
            var checkRole = _unitOfWork.RoleRepository.GetAllAsync().Result.FirstOrDefault(x => x.Name.Equals(name) && x.IsDeleted == false);

            if (checkRole is not null)
                throw new Exception("Role không được trùng.");

            await _unitOfWork.RoleRepository.AddAsync(new Role { Name = name});

            return await _unitOfWork.SaveChangeAsync() > 0 ? true : throw new Exception("Tạo role thất bại");
        }

        public async Task<List<RoleViewModel>> GetRole()
        {
            var role = await _unitOfWork.RoleRepository.GetAllAsync();

            var mapper = _mapper.Map<List<RoleViewModel>>(role);

            return mapper;
        }
    }
}
