﻿using Application.Interfaces;
using Application.ViewModels.AnimalProfileViewModels;
using Application.ViewModels.ArticleViewModels;
using Application.ViewModels.FoodViewModels;
using Application.ViewModels.UserViewModels;
using AutoMapper;
using Domain.Entities;
using Domain.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Services
{
    public class ArticleService : IArticleService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly ICurrentTime _currentTime;
        private readonly IMapper _mapper;
        private readonly IClaimsService _claimsService;

        public ArticleService(IUnitOfWork unitOfWork, ICurrentTime currentTime, IMapper mapper, IClaimsService claimsService)
        {
            _unitOfWork = unitOfWork;
            _currentTime = currentTime;
            _mapper = mapper;
            _claimsService = claimsService;
        } 

        public List<ArticleViewModelDetail> GetArticle()
        {
            var article = _unitOfWork.ArticleRepository.GetAllAsync().Result.Where(x => x.IsDeleted == false).ToList();

            List<ArticleViewModelDetail> list = new List<ArticleViewModelDetail>();

            foreach (var item in article)
            {
                var mapper = _mapper.Map<ArticleViewModelDetail>(item);
                mapper.User = _mapper.Map<UserViewModel>(item.User);
                list.Add(mapper);
            }
            return list;
        }

        public async Task<ArticleViewModelDetail> GetArticleById(Guid id)
        {
            var article = await _unitOfWork.ArticleRepository.GetByIdAsync(id);

            if (article is null || article.IsDeleted)
                throw new Exception("Không tìm thấy bài đăng");

            var mapper = _mapper.Map<ArticleViewModelDetail>(article);
            mapper.User = _mapper.Map<UserViewModel>(article.User);

            return mapper;
        }

        public List<ArticleViewModelDetail> GetArticleByTitle(string title)
        {
            var article = _unitOfWork.ArticleRepository.GetAllAsync().Result.Where(x => x.IsDeleted == false && x.Title.Contains(title)).ToList();

            if (article.Count == 0)
                throw new Exception("Không tìm thấy bài đăng");

            ICollection<ArticleViewModelDetail> articleViewModel = new List<ArticleViewModelDetail>();
            foreach (var item in article)
            {
                var viewModel = _mapper.Map<ArticleViewModelDetail>(item);
                viewModel.User = _mapper.Map<UserViewModel>(item.User);

                articleViewModel.Add(viewModel);
            }
            return (List<ArticleViewModelDetail>)articleViewModel;
        }

        public async Task<ArticleViewModel> CreateArticle(CreateArticleViewModel createArticle)
        {
            var mapper = _mapper.Map<Article>(createArticle);

            mapper.ArticleStatus = ArticleStatus.Created;

            await _unitOfWork.ArticleRepository.AddAsync(mapper);

            mapper.UserId = _claimsService.GetCurrentUserId;

            return await _unitOfWork.SaveChangeAsync() > 0 ? _mapper.Map<ArticleViewModel>(mapper) : throw new Exception("Tạo bài đăng thất bại");
        }

        public async Task<bool> RemoveArticle(Guid Id)
        {
            var findArticle = await _unitOfWork.ArticleRepository.GetByIdAsync(Id);

            if (findArticle is null || findArticle.IsDeleted)
            {
                throw new Exception("Không tìm thấy bài đăng.");
            }

            _unitOfWork.ArticleRepository.Remove(findArticle);

            return await _unitOfWork.SaveChangeAsync() > 0 ? true : false;

        }

        public async Task<ArticleViewModel> UpdateArticle(UpdateArticleViewModel articleViewModel)
        {
            var findArticle = await _unitOfWork.ArticleRepository.GetByIdAsync(articleViewModel.Id);

            if (findArticle is null || findArticle.IsDeleted)
            {
                throw new Exception("Không tìm thấy bài đăng.");
            }

            var mapper = _mapper.Map(articleViewModel, findArticle);
            //update article thì phải update lại status để duyệt lại, tránh trường hợp bài sau khi đã duyệt chỉnh lung tung
            mapper.ArticleStatus = ArticleStatus.Created;
            _unitOfWork.ArticleRepository.Update(mapper);

            return await _unitOfWork.SaveChangeAsync() > 0 ? _mapper.Map<ArticleViewModel>(findArticle) : throw new Exception("Cập nhật bài đăng thất bại");
        }

        public async Task<ArticleViewModel> UpdateArticleStatus(UpdateStatusArticleViewModel model)
        {
            var findArticle = await _unitOfWork.ArticleRepository.GetByIdAsync(model.Id);

            if (findArticle is null || findArticle.IsDeleted)
            {
                throw new Exception("Không tìm thấy bài đăng.");
            }

            if (!Enum.IsDefined(typeof(ArticleStatus), model.ArticleStatus)) throw new Exception("Không tìm thấy trạng thái cần cập nhật");

            findArticle.ArticleStatus = (ArticleStatus)model.ArticleStatus;

            _unitOfWork.ArticleRepository.Update(findArticle);

            return await _unitOfWork.SaveChangeAsync() > 0 ? _mapper.Map<ArticleViewModel>(findArticle) : throw new Exception("Cập nhật trạng thái bài đăng thất bại");
        }

        public async Task<List<ArticleViewModelDetail>> SearchArticles(SearchArticleViewModel model)
        {
            var searchResult = await _unitOfWork.ArticleRepository.SearchArticle(model);

            if (model.FullUserName == null 
                && model.CreateDate == null 
                && model.Title == null 
                && model.ArticleStatus == null
                && model.Body == null
                && model.UserId == null)

                searchResult = _unitOfWork.ArticleRepository.GetAllAsync().Result.Where(x => x.IsDeleted == false).ToList();

            ICollection<ArticleViewModelDetail> articleViewModel = new List<ArticleViewModelDetail>();
            foreach (var item in searchResult)
            {
                var viewModel = _mapper.Map<ArticleViewModelDetail>(item);
                viewModel.User =  _mapper.Map<UserViewModel>(item.User);

                articleViewModel.Add(viewModel);
            }
            return (List<ArticleViewModelDetail>)articleViewModel;


        }

    }
}
