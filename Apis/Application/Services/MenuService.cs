﻿using Application.Interfaces;
using Application.ViewModels.AnimalProfileViewModels;
using Application.ViewModels.CageViewModels;
using Application.ViewModels.FoodViewModels;
using Application.ViewModels.MealViewModels;
using Application.ViewModels.MenuViewModels;
using Application.ViewModels.PieceViewModels;
using Application.ViewModels.ProfileHistoryViewModels;
using Application.ViewModels.UserViewModels;
using AutoMapper;
using Domain.Entities;
using Domain.Enums;


namespace Application.Services
{
    public class MenuService : IMenuService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly ICurrentTime _currentTime;
        private readonly IClaimsService _claimsService;
        private readonly IMapper _mapper;

        public MenuService(IUnitOfWork unitOfWork, ICurrentTime currentTime, IClaimsService claimsService, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _currentTime = currentTime;
            _claimsService = claimsService;
            _mapper = mapper;
        }

        public async Task<MenuViewModel> CreateMenu(CreateMenuViewModel menuView)
        {
            var checkMenu = _unitOfWork.MenuRepository.GetAllAsync().Result.Where(x => x.AnimalProfileId == menuView.AnimalProfileId && x.StartDate == menuView.StartDate).ToList();

            if (checkMenu.Count != 0)
            {
                var a = checkMenu.Where(x => x.Name.Equals(menuView.Name) || x.Slot.Equals(menuView.Slot)).ToList();

                if (a.Count != 0)
                {
                    throw new Exception("Tên Menu không được trùng hoặc bữa ăn không được trùng");
                }//Tạo
            }

            var findFood = await _unitOfWork.FoodRepository.GetByIdAsync(menuView.FoodId);

            if (findFood is null || findFood.IsDeleted == true)
            {
                throw new Exception("Không tìm thấy thực phẩm.");
            }

            var findAnimal = await _unitOfWork.AnimalProfileRepository.GetByIdAsync(menuView.AnimalProfileId);

            if (findAnimal is null || findAnimal.IsDeleted == true)
            {
                throw new Exception("Không tìm thấy thú.");
            }

            var mapper = _mapper.Map<Menu>(menuView);

            if (!Enum.IsDefined(typeof(Slot), menuView.Slot)) throw new Exception("Không tìm thấy bữa ăn.");

            mapper.Slot = (Slot)menuView.Slot;

            await _unitOfWork.MenuRepository.AddAsync(mapper);

            var food = await _unitOfWork.FoodRepository.GetByIdAsync(menuView.FoodId);

            var menu = _mapper.Map<MenuViewModel>(mapper);
            menu.NameFood = food.Name;

            return await _unitOfWork.SaveChangeAsync() > 0 ? menu : throw new Exception("Tạo menu thất bại");
        }

        public async Task<List<MenuViewModel>> GetMenu()
        {
            var menu = await _unitOfWork.MenuRepository.GetAllAsync();

            var mapper = _mapper.Map<List<MenuViewModel>>(menu);

            return mapper;
        }

        public async Task<List<MenuViewModel>> GetMenuByIdUser()
        {
            var menu = _unitOfWork.MenuRepository.GetAllAsync().Result.Where(x => x.CreatedBy == _claimsService.GetCurrentUserId).ToList();

            var mapper = _mapper.Map<List<MenuViewModel>>(menu);

            return mapper;
        }

        public async Task<List<MenuViewModel>> GetMenuByIdAnimal(Guid AnimalId)
        {
            var menu = _unitOfWork.MenuRepository.GetAllAsync().Result.Where(x => x.AnimalProfileId == AnimalId).ToList();

            List<Menu> menus = new List<Menu>();

            var date = _currentTime.GetCurrentTime();

            menus = menu.Where(x => x.StartDate.Value.Day == date.Day &&
                                    x.StartDate.Value.Month == date.Month &&
                                    x.StartDate.Value.Year == date.Year).ToList();

            if (menus.Count != 0)
            {
                return CheckMealIsExist(menus) ? throw new Exception("Thú đã được cho ăn.") : _mapper.Map<List<MenuViewModel>>(menus);
            }
            else
            {
                menus = menu.Where(x => x.StartDate < _currentTime.GetCurrentTime()).ToList();
                menus.Max(x => x.StartDate);
                return CheckMealIsExist(menus) ? throw new Exception("Thú đã được cho ăn.") : _mapper.Map<List<MenuViewModel>>(menus);
            }
        }

        public bool CheckMealIsExist(List<Menu> menus)
        {
            //check nếu IDMenu này đã xuất hiện bên meal vào ngày hôm nay thì ko hiển thị menu nữa
            List<Guid> guids = new List<Guid>();

            var date = _currentTime.GetCurrentTime();

            foreach (var item in menus.Select(x => x.Id))
            {
                if (_unitOfWork.MealRepository.GetAllAsync().Result.FirstOrDefault(x => x.MenuId == item &&
                                                                                        x.Date.Day == date.Day &&
                                                                                        x.Date.Month == date.Month &&
                                                                                        x.Date.Year == date.Year) != null)
                    guids.Add(item);
            }

            return guids.Count != 0 ? true : false;
        }

        public async Task<bool> RemoveMenu(Guid Id)
        {
            var findMenu = await _unitOfWork.MenuRepository.GetByIdAsync(Id);

            if (findMenu is null || findMenu.IsDeleted)
            {
                throw new Exception("Không tìm thấy menu.");
            }

            _unitOfWork.MenuRepository.Remove(findMenu);

            return await _unitOfWork.SaveChangeAsync() > 0 ? true : false;
        }

        public async Task<MenuViewModel> UpdateMenu(UpdateMenuViewModel menuView)
        {
            var findMenu = await _unitOfWork.MenuRepository.GetByIdAsync(menuView.Id);

            if (findMenu is null || findMenu.IsDeleted)
            {
                throw new Exception("Không tìm thấy menu.");
            }

            _unitOfWork.MenuRepository.Update(_mapper.Map(menuView, findMenu));

            var food = await _unitOfWork.FoodRepository.GetByIdAsync(findMenu.FoodId);

            var menu = _mapper.Map<MenuViewModel>(findMenu);
            menu.NameFood = food.Name;

            return await _unitOfWork.SaveChangeAsync() > 0 ? menu : throw new Exception("Cập nhật menu thất bại");
        }

        public async Task<List<MenuViewModelDetail>> SearchMenu(SearchMenuModel model)
        {
            ICollection<MenuViewModelDetail> menuViewModelDetail = new List<MenuViewModelDetail>();
            var searchResult = await _unitOfWork.MenuRepository.SearchMenu(model);

            /*if (searchResult.Count == 0)
                await GetMenu();*/

            foreach (var item in searchResult)
            {
                var viewModel = _mapper.Map<MenuViewModelDetail>(item);

                viewModel.Food = _mapper.Map<FoodViewModel>(item.Food);
                viewModel.AnimalProfile = _mapper.Map<AnimalProfileViewModel>(item.AnimalProfile);
                viewModel.Meals = _mapper.Map<List<MealViewModel>>(item.Meals);

                menuViewModelDetail.Add(viewModel);
            }

            return _mapper.Map<List<MenuViewModelDetail>>(searchResult);
        }

        public async Task<List<MenuViewModel>> GetAllMenuByIdAnimal(Guid AnimalId)
        {
            var menu = _unitOfWork.MenuRepository.GetAllAsync().Result.Where(x => x.AnimalProfileId == AnimalId).OrderByDescending(x => x.StartDate).ToList();

            return _mapper.Map<List<MenuViewModel>>(menu);
        }
    }

}
