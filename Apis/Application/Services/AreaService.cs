﻿using Application.Interfaces;
using Application.Repositories;
using Application.ViewModels.AreaViewModels;
using Application.ViewModels.UserViewModels;
using AutoMapper;
using Domain.Entities;

namespace Application.Services
{
    public class AreaService : IAreaService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly ICurrentTime _currentTime;
        private readonly IClaimsService _claimsService;
        private readonly IMapper _mapper;
        private readonly IAreaRepository _areaRepository;

        public AreaService(IUnitOfWork unitOfWork, ICurrentTime currentTime, IClaimsService claimsService, IMapper mapper, IAreaRepository areaRepository)
        {
            _unitOfWork = unitOfWork;
            _currentTime = currentTime;
            _claimsService = claimsService;
            _mapper = mapper;
            _areaRepository = areaRepository;
        }

        public async Task<List<AreaViewModelDetail>> GetArea()
        {
            ICollection<AreaViewModelDetail> areaViews = new List<AreaViewModelDetail>();
            var areas = _unitOfWork.AreaRepository.GetAllAsync().Result.Where(x => x.IsDeleted == false)
                .OrderByDescending(x => x.CreationDate).ToList();

            foreach (var item in areas)
            {
                var mapper = _mapper.Map<AreaViewModelDetail>(item);
                mapper.User = _mapper.Map<UserViewModel>(item.User);
                areaViews.Add(mapper);
            }

             return (List<AreaViewModelDetail>)areaViews;
        }

        public async Task<AreaViewModelDetail> GetAreaById(Guid id)
        {
            var area = await _unitOfWork.AreaRepository.GetByIdAsync(id);
            if (area is null || area.IsDeleted)
            {
                throw new Exception("Không tìm thấy khu vực");
            }
            var viewModel = _mapper.Map<AreaViewModelDetail>(area);
            viewModel.User = _mapper.Map<UserViewModel>(area.User);
            return viewModel;
        }

        public async Task<AreaViewModel> CreateArea(CreateAreaViewModels createArea)
        {
            var checkName = _unitOfWork.AreaRepository.GetAllAsync().Result.Where(x => x.Name.ToLower().Equals(createArea.Name.ToLower()) && x.IsDeleted == false).ToList();
            if (checkName.Count() != 0)
                throw new Exception("Tên khu vực không được trùng");

            CreateAreaViewModel areaView = new()
            {
                Name = createArea.Name,
                UserId = _claimsService.GetCurrentUserId
            };

            var mapper = _mapper.Map<Area>(areaView);

            await _unitOfWork.AreaRepository.AddAsync(mapper);

            return await _unitOfWork.SaveChangeAsync() > 0 ? _mapper.Map<AreaViewModel>(mapper) : throw new Exception("Tạo khu vực thất bại");

        }

        public async Task<AreaViewModel> UpdateArea(UpdateAreaViewModel model)
        {
            var getArea = _unitOfWork.AreaRepository.GetAllAsync().Result.Where(x => x.IsDeleted == false);
            
            var findArea = getArea.FirstOrDefault(x => x.Id == model.Id);


            if (findArea is not null)
            {
                if (getArea.Where(x => x.Name.ToLower().Equals(model.Name.ToLower())).ToList().Count == 0)
                {
                    findArea.Name = model.Name;
                    _unitOfWork.AreaRepository.Update(findArea);

                    return await _unitOfWork.SaveChangeAsync() > 0 ? _mapper.Map<AreaViewModel>(findArea) : throw new Exception("Cập nhật khu vực thất bại");
                }
                else
                {
                    throw new Exception("Tên khu vực không được trùng");
                }

            }
            throw new Exception("Không tìm thấy khu vực.");
        }

        public async Task<bool> RemoveArea(Guid Id)
        {
            var findArea = _areaRepository.getAreaByIdWithRelation(Id);

            if (findArea is not null && findArea.IsDeleted == false)
            {
                if (findArea.Cages is null)
                {
                    _unitOfWork.AreaRepository.Remove(findArea);

                    return await _unitOfWork.SaveChangeAsync() > 0 ? true : false;
                }
                else
                {
                    throw new Exception("Xóa thất bại, hiện khu vực này vẫn còn chuồng đang được sử dụng.");
                }
            }

            throw new Exception("Không tìm thấy khu vực.");

        }

    }
}
