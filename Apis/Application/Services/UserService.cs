﻿using Application;
using Application.Interfaces;
using Application.Utils;
using Application.ViewModels.AuthViewModel;
using Application.ViewModels.FoodViewModels;
using Application.ViewModels.UserViewModels;
using AutoMapper;
using Domain.Entities;
using Microsoft.Extensions.Configuration;
using System.Net.Mail;
using System.Net;
using System.Text;

namespace Infrastructures.Services
{
    public class UserService : IUserService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private readonly IClaimsService _claimsService;
        private readonly ICurrentTime _currentTime;
        private readonly IConfiguration _configuration;

        public UserService(IUnitOfWork unitOfWork, IMapper mapper, ICurrentTime currentTime, IConfiguration configuration, IClaimsService claimsService)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
            _currentTime = currentTime;
            _configuration = configuration;
            _claimsService = claimsService;
        }

        public async Task<List<UserViewModel>> GetUser()
        {
            var listUser = _unitOfWork.UserRepository.GetAllAsync().Result.Where(x => !x.Role.Name.Equals("Admin")).ToList();

            var mapper = _mapper.Map<List<UserViewModel>>(listUser);

            return mapper;
        }

        public async Task<UserViewModel> GetUserById(Guid id)
        {
            var listUser = await _unitOfWork.UserRepository.GetByIdAsync(id);

            var mapper = _mapper.Map<UserViewModel>(listUser);

            return mapper;
        }

        public async Task<LoginViewModel> LoginAsync(UserLoginViewModel userObject)
        {
            var user = await _unitOfWork.UserRepository.GetUserByUserNameAndPasswordHash(userObject.UserName, userObject.Password.Hash());
            if (user == null)
            {
                throw new Exception("Tên đăng nhập hoặc mật khẩu không chính xác.");
            }

            var token = user.GenerateJsonWebToken(_configuration["AppSettings:SecretKey"]);

            int roleNumber = 1;

            switch (user.Role.Name)
            {
                case "Admin":
                    roleNumber = 1;
                    break;

                case "Staff":
                    roleNumber = 2;
                    break;

                case "Manager":
                    roleNumber = 3;
                    break;

                case "ZooTrainer":
                    roleNumber = 4;
                    break;

                case "User":
                    roleNumber = 5;
                    break;
            }



            return new LoginViewModel
            {
                Id = user.Id,
                Username = user.UserName,
                FullName = user.FullName,
                Email = user.Email,
                Avatar = user.Avatar,
                Token = token,
                Role = user.Role.Name,
                RoleNumber = roleNumber
            };
        }

        public async Task RegisterAsync(RegisterModel userObject)
        {
            // check username exited
            var isExited = await _unitOfWork.UserRepository.CheckUserNameExited(userObject.UserName);
            var userByEmail = await _unitOfWork.UserRepository.GetUserByEmail(userObject.Email);

            var checkRole = _unitOfWork.RoleRepository.GetAllAsync().Result.Find(x => x.Name.Equals("User"));

            if (isExited)
            {
                throw new Exception("Username đã xuất hiện vui lòng nhập lại.");
            }

            if (userByEmail != null)
            {
                throw new Exception("Email này đã được sử dụng, vui lòng nhập lại.");
            }

            if (checkRole is null)
                throw new Exception("Không tìm thấy Role.");

            var newUser = new User
            {
                UserName = userObject.UserName,
                FullName = userObject.FullName,
                Email = userObject.Email,
                Avatar = userObject.Avatar,
                Phone = userObject.Phone,
                PasswordHash = userObject.Password.Hash(),
                RoleId = checkRole.Id,
                CreationDate = _currentTime.GetCurrentTime(),
            };

            await _unitOfWork.UserRepository.AddAsync(newUser);
            await _unitOfWork.SaveChangeAsync();
        }

        public async Task RegisterEmployeeAsync(RegisterEmployeeModel userObject)
        {
            // check username exited
            var isExited = await _unitOfWork.UserRepository.CheckUserNameExited(userObject.UserName);
            var userByEmail = await _unitOfWork.UserRepository.GetUserByEmail(userObject.Email);

            var checkRole = await _unitOfWork.RoleRepository.GetByIdAsync(userObject.RoleId);

            if (isExited)
            {
                throw new Exception("Username đã xuất hiện vui lòng nhập lại.");
            }

            if (userByEmail != null)
            {
                throw new Exception("Email này đã được sử dụng, vui lòng nhập lại.");
            }

            if (checkRole is null)
                throw new Exception("Không tìm thấy Role.");

            var newUser = new User
            {
                UserName = userObject.UserName,
                FullName = userObject.FullName,
                Email = userObject.Email,
                Avatar = userObject.Avatar,
                Phone = userObject.Phone,
                PasswordHash = userObject.Password.Hash(),
                RoleId = checkRole.Id,
                CreationDate = _currentTime.GetCurrentTime(),
                CreatedBy = _claimsService.GetCurrentUserId
            };

            await _unitOfWork.UserRepository.AddAsync(newUser);
            await _unitOfWork.SaveChangeAsync();
        }

        public async Task<bool> RemoveUser(Guid Id)
        {
            var findUser = await _unitOfWork.UserRepository.GetByIdAsync(Id);


            if (findUser is not null && findUser.IsDeleted == false)
            {
                _unitOfWork.UserRepository.Remove(findUser);

                return await _unitOfWork.SaveChangeAsync() > 0 ? true : false;
            }

            throw new Exception("Không tìm thấy người dùng.");
        }

        public async Task<UserViewModel> UpdateUser(UpdateUserViewModel updateUserView)
        {
            var findUser = await _unitOfWork.UserRepository.GetByIdAsync(updateUserView.Id);
            var listUser = _unitOfWork.UserRepository.GetAllAsync().Result.Where(x => x.IsDeleted == false && x.Email == updateUserView.Email).ToList();

            if (findUser is not null && findUser.IsDeleted == false)
            {
                if (updateUserView.Email != findUser.Email) // có thay đổi mail
                {
                    // check trùng mail với những user khác không
                    if (listUser.Count > 0)
                    {
                        throw new Exception("Email này đã được sử dụng, vui lòng sử dụng Email khác.");
                    }
                }
                //updateUserView.PasswordHash.Hash();

                var mapper = _mapper.Map(updateUserView, findUser);

                _unitOfWork.UserRepository.Update(mapper);

                var mapperv2 = _mapper.Map<UserViewModel>(findUser);

                return await _unitOfWork.SaveChangeAsync() > 0 ? mapperv2 : throw new Exception("Cập nhật người dùng thất bại");
            }

            throw new Exception("Không tìm thấy người dùng.");
        }
        public async Task<UserViewModel> UpdateRoleUser(UpdateUserRoleViewModel updateUserRoleViewModel)
        {
            var user = await _unitOfWork.UserRepository.GetByIdAsync(updateUserRoleViewModel.Id);

            if (user is null) { throw new Exception("Không tìm thấy Id: "+ updateUserRoleViewModel.Id); }

            user.RoleId = updateUserRoleViewModel.RoleId;

            _unitOfWork.SaveChangeAsync();

            return _mapper.Map<UserViewModel>(await _unitOfWork.UserRepository.GetByIdAsync(updateUserRoleViewModel.Id));
        }

        public async Task<UserViewModel> ChangePassword(ChangePasswordModel model)
        {
            var findUser = await _unitOfWork.UserRepository.GetByIdAsync(model.UserId);
            if (findUser is not null && findUser.IsDeleted == false)
            {
                if (model.OldPassword.Hash() == findUser.PasswordHash)
                {
                    if (model.NewPassword == model.ConfirmPassword)
                    {
                        findUser.PasswordHash = model.NewPassword.Hash();
                        _unitOfWork.UserRepository.Update(findUser);

                        return await _unitOfWork.SaveChangeAsync() > 0 ? _mapper.Map<UserViewModel>(findUser) : throw new Exception("Thay đổi mật khẩu thất bại");
                    }
                    throw new Exception("Mật khẩu xác nhận không khớp");
                }
                throw new Exception("Mật khẩu cũ không đúng");
            }
            throw new Exception("Không tìm thấy người dùng)");
        }

        public async Task<UserViewModel> ResetPassword(ResetPasswordModel model)
        {
            var findUser = await _unitOfWork.UserRepository.GetByIdAsync(model.Id);
            if (findUser is not null && findUser.IsDeleted == false)
            {
                findUser.PasswordHash = model.NewPassword.Hash();

                _unitOfWork.UserRepository.Update(findUser);
                return await _unitOfWork.SaveChangeAsync() > 0 ? _mapper.Map<UserViewModel>(findUser) : throw new Exception("Đặt lại mật khẩu thất bại");
            }
            throw new Exception("Không tìm thấy người dùng)");
        }

        public async Task<bool> ForgotPassword(ForgotPasswordModel model)
        {
            //gọi api tạo otp và send qua email
            //user nhập lại pass mới kèm theo otp
            //check otp và update pass

            var user = await _unitOfWork.UserRepository.GetUserByEmail(model.Email)
                ?? throw new Exception("Không tìm thấy tài khoản của bạn, hãy đảm bảo bạn nhập đúng Email");

            if (model.NewPassword == model.ConfirmPassword)
            {
                if (model.OTP == user.OTP)
                {
                    user.PasswordHash = model.NewPassword.Hash();
                    _unitOfWork.UserRepository.Update(user);

                    return await _unitOfWork.SaveChangeAsync() > 0 ? true : false;
                }
                throw new Exception("Mã OTP không đúng");
            }
            throw new Exception("Mật khẩu xác nhận chưa trùng khớp");
        }

        public async Task<bool> SendEmail(SendEmailModel model)
        {
            var user = await _unitOfWork.UserRepository.GetUserByEmail(model.Email);

            if (user == null) throw new Exception("Không tìm thấy tài khoản của bạn, hãy đảm bảo bạn nhập đúng Email");
            //tạo string random otp
            Random random = new Random();
            StringBuilder sb = new StringBuilder();

            for (int i = 0; i < 6; i++)
            {
                // Sinh số ngẫu nhiên từ 0 đến 9 và ghép vào chuỗi
                int randomNumber = random.Next(0, 10);
                sb.Append(randomNumber);
            }

            var otp = sb.ToString();

            //gửi opt qua mail
            string emailForSend = "testsendemail480@gmail.com"; // tạo ra 1 email giả để mà làm
            string appPasswordConfiguration = "aipzfxfjkumsiklw"; // tạo 2FA rồi sau đó bật cái app password lên

            var smtpClient = new SmtpClient
            {
                Port = 587,
                EnableSsl = true,
                Host = "smtp.gmail.com",
                UseDefaultCredentials = false,
                Credentials = new NetworkCredential(emailForSend, appPasswordConfiguration),
            };

            var message = new MailMessage()
            {
                Subject = "Saigon Zoo",
                Body = "Bạn vừa yêu cầu thay đổi mật khẩu, hãy báo với chúng tôi nếu đó không phải là bạn. " +
                "Vui lòng không chia sẻ mã OTP cho bất kỳ ai khác. Mã OTP của bạn là: " + otp,
                From = new MailAddress(emailForSend),
            };
            message.To.Add(new MailAddress(model.Email)); // muốn gửi cho ai thì điền vô đây
            message.To.Add(new MailAddress("nghiapdse150938@fpt.edu.vn")); // thêm vài người nữa

            await smtpClient.SendMailAsync(message);

            user.OTP = otp;

            _unitOfWork.UserRepository.Update(user);
            await _unitOfWork.SaveChangeAsync();

            return true;
        }


    }
}
