﻿using Application.Interfaces;
using Application.Repositories;
using Application.ViewModels.AreaViewModels;
using Application.ViewModels.CageViewModels;
using AutoMapper;
using Domain.Entities;

namespace Application.Services
{
    public class CageService : ICageService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly ICurrentTime _currentTime;
        private readonly IClaimsService _claimsService;
        private readonly IMapper _mapper;
        private readonly ICageRepository _cageRepository;

        public CageService(IUnitOfWork unitOfWork, ICurrentTime currentTime, IClaimsService claimsService, IMapper mapper, ICageRepository cageRepository)
        {
            _unitOfWork = unitOfWork;
            _currentTime = currentTime;
            _claimsService = claimsService;
            _mapper = mapper;
            _cageRepository = cageRepository;
        }

        public List<CageViewModel> GetCage()
        {
            var mapper = _mapper.Map<List<CageViewModel>>(_unitOfWork.CageRepository.GetAllAsync().Result.Where(x => x.IsDeleted == false)
                .OrderByDescending(x => x.CreationDate));

            return mapper;
        }

        public async Task<CageViewModel> GetCageById(Guid id)
        {
            var cage = await _unitOfWork.CageRepository.GetByIdAsync(id);
            if (cage is null || cage.IsDeleted)
            {
                throw new Exception("Không tìm thấy chuồng");
            }
            var viewModel = _mapper.Map<CageViewModel>(cage);
            return viewModel;
        }

        public async Task<CageViewModel> CreateCage(CreateCageViewModel model)
        {
            var checkName = _unitOfWork.CageRepository.GetAllAsync().Result.Where(x => x.Name.ToLower().Equals(model.Name.ToLower()) && x.IsDeleted == false).ToList();
            if (checkName.Count != 0)
                throw new Exception("Tên chuồng không được trùng");

            /*CreateCageViewModel CageView = new()
            {
                Name = name,
                UserId = _claimsService.GetCurrentUserId
            };*/

            var mapper = _mapper.Map<Cage>(model);
            var count = await _unitOfWork.CageRepository.GetAllAsync();
            mapper.CageNumber = "C" + (count.Count + 1);

            await _unitOfWork.CageRepository.AddAsync(mapper);

            return await _unitOfWork.SaveChangeAsync() > 0 ? _mapper.Map<CageViewModel>(mapper) : throw new Exception("Tạo chuồng thất bại");

        }

        public async Task<CageViewModel> UpdateCage(UpdateCageViewModel model)
        {
            var findCage = await _unitOfWork.CageRepository.GetByIdAsync(model.Id);
            var listCage = await _unitOfWork.CageRepository.GetAllAsync();

            if (findCage is not null && findCage.IsDeleted == false)
            {
                if (!findCage.Name.ToLower().Equals(model.Name.ToLower())) //co thay doi name
                {
                    //check trùng name, được update tên cũ, update tên mới không trùng với tên chưa xóa
                    if (listCage.Where(x=>x.Name.ToLower().Equals(model.Name.ToLower()) && x.IsDeleted == false).ToList().Count != 0)
                    {
                        throw new Exception("Tên chuồng đã tồn tại.");
                    }
                }
                
                var mapper = _mapper.Map(model, findCage);
                _unitOfWork.CageRepository.Update(mapper);

                return await _unitOfWork.SaveChangeAsync() > 0 ? _mapper.Map<CageViewModel>(mapper) : throw new Exception("Cập nhật chuồng thất bại");
            }
            throw new Exception("Không tìm thấy chuồng.");
        }

        public async Task<bool> RemoveCage(Guid Id)
        {
            var findCage = _cageRepository.getCageByIdWithRelation(Id);

            if (findCage is not null && findCage.IsDeleted == false)
            {
                if (findCage.AnimalProfiles is null)
                {
                    _unitOfWork.CageRepository.Remove(findCage);

                    return await _unitOfWork.SaveChangeAsync() > 0 ? true : false;
                }
                else
                {
                    throw new Exception("Xóa thất bại, hiện chuồng này vẫn còn thú hoạt động");
                }
            }

            throw new Exception("Không tìm thấy chuồng.");

        }

        public async Task<List<CageViewModel>> GetCageByAreaID(Guid id)
        {
            var cage = _unitOfWork.CageRepository.GetAllAsync().Result.Where(x => x.AreaId == id).ToList();

            var viewModel = _mapper.Map<List<CageViewModel>>(cage);
            foreach (var item in viewModel)
            {
                item.Area = _mapper.Map<AreaViewModel>(item.Area);
            }
            return viewModel;
        }
    }
}
