﻿using Application.Interfaces;
using Application.Repositories;
using Application.ViewModels.PieceViewModels;
using AutoMapper;
using Domain.Entities;

namespace Application.Services
{
    public class PieceService : IPieceService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly ICurrentTime _currentTime;
        private readonly IClaimsService _claimsService;
        private readonly IMapper _mapper;
        private readonly IPieceRepository _pieceRepository;

        public PieceService(IUnitOfWork unitOfWork, ICurrentTime currentTime, IClaimsService claimsService, IMapper mapper, IPieceRepository pieceRepository)
        {
            _unitOfWork = unitOfWork;
            _currentTime = currentTime;
            _claimsService = claimsService;
            _mapper = mapper;
            _pieceRepository = pieceRepository;
        }

        public List<PieceViewModel> GetPiece()
        {
            var mapper = _mapper.Map<List<PieceViewModel>>(_unitOfWork.PieceRepository.GetAllAsync().Result.Where(x => x.IsDeleted == false)
                .OrderByDescending(x => x.CreationDate));

            return mapper;
        }

        public async Task<PieceViewModel> GetPieceById(Guid id)
        {
            var piece = await _unitOfWork.PieceRepository.GetByIdAsync(id);
            if (piece is null || piece.IsDeleted)
            {
                throw new Exception("Không tìm thấy loài");
            }
            var viewModel = _mapper.Map<PieceViewModel>(piece);
            return viewModel;
        }
         
        public async Task<PieceViewModel> CreatePiece(CreatePieceViewModel createPieceView)
        {
            var checkName = _unitOfWork.PieceRepository.GetAllAsync().Result.Where(x => x.Name.ToLower().Equals(createPieceView.Name.ToLower()) && x.IsDeleted == false).ToList();
            
            if (checkName.Count != 0)
                throw new Exception("Tên loài không được trùng");

            var mapper = _mapper.Map<Piece>(createPieceView);

            await _unitOfWork.PieceRepository.AddAsync(mapper);

            return await _unitOfWork.SaveChangeAsync() > 0 ? _mapper.Map<PieceViewModel>(mapper) : throw new Exception("Tạo loài thất bại");

        }

        public async Task<PieceViewModel> UpdatePiece(UpdatePieceViewModel updatePieceView)
        {
            var findPiece = await _unitOfWork.PieceRepository.GetByIdAsync(updatePieceView.Id);
            var listPiece = await _unitOfWork.PieceRepository.GetAllAsync();

            if (findPiece is not null && findPiece.IsDeleted == false)
            {
                /*if (!updatePieceView.Name.Equals(findPiece.Name))
                {*/
                    /*findPiece.Name = updatePieceView.Name;
                _mapper.Map(updatePieceView, findPiece);*/
                    if (!findPiece.Name.ToLower().Equals(updatePieceView.Name.ToLower())) //co thay doi name
                    {
                        if (listPiece.Where(x => x.Name.ToLower().Equals(updatePieceView.Name.ToLower())).ToList().Count != 0)
                        {
                            throw new Exception("Tên loài đã tồn tại.");
                        }
                    }
                    _unitOfWork.PieceRepository.Update(_mapper.Map(updatePieceView, findPiece));

                    return await _unitOfWork.SaveChangeAsync() > 0 ? _mapper.Map<PieceViewModel>(findPiece) : throw new Exception("Cập nhật loài thất bại");
                /*}
                else
                {
                    throw new Exception("Cập nhật loài không được trùng");
                }*/

            }
            throw new Exception("Không tìm thấy loài.");
        }

        public async Task<bool> RemovePiece(Guid Id)
        {
            var findPiece = _pieceRepository.getPieceByIdWithRelation(Id);
            if (findPiece is not null && findPiece.IsDeleted == false)
            {
                if (findPiece.AnimalProfiles is null)
                {
                    _unitOfWork.PieceRepository.Remove(findPiece);

                    return await _unitOfWork.SaveChangeAsync() > 0 ? true : false;
                }
                else
                {
                    throw new Exception("Xóa thất bại, hiện loài này vẫn còn thú tồn tại");
                }
            }

            throw new Exception("Không tìm thấy loài.");

        }

    }
}
