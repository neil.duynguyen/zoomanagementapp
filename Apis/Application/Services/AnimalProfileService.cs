﻿using Application.Interfaces;
using Application.Repositories;
using Application.ViewModels.AnimalProfileViewModels;
using Application.ViewModels.CageViewModels;
using Application.ViewModels.MenuViewModels;
using Application.ViewModels.PieceViewModels;
using Application.ViewModels.ProfileHistoryViewModels;
using Application.ViewModels.UserViewModels;
using AutoMapper;
using Domain.Entities;
using Domain.Enums;
using Microsoft.VisualBasic;
using System;
using System.Collections.Generic;
using System.Text.Json;

namespace Application.Services
{
    public class AnimalProfileService : IAnimalProfileService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly ICurrentTime _currentTime;
        private readonly IClaimsService _claimsService;
        private readonly IMapper _mapper;
        private readonly IAnimalProfileRepository _animalProfileRepository;
        private readonly IUserRepository _userRepository;

        public AnimalProfileService(IUnitOfWork unitOfWork, ICurrentTime currentTime, IClaimsService claimsService, IMapper mapper, IAnimalProfileRepository animalProfileRepository, IUserRepository userRepository)
        {
            _unitOfWork = unitOfWork;
            _currentTime = currentTime;
            _claimsService = claimsService;
            _mapper = mapper;
            _animalProfileRepository = animalProfileRepository;
            _userRepository = userRepository;
        }

        public async Task<List<AnimalProfileViewModelDetail>> GetAnimalProfile()
        {
            ICollection<AnimalProfileViewModelDetail> animalProfileViewModel = new List<AnimalProfileViewModelDetail>();
            var animalProfiles = _unitOfWork.AnimalProfileRepository.GetAllAsync().Result.Where(x => x.IsDeleted == false).OrderByDescending(x => x.CreationDate).ToList();

            foreach (var item in animalProfiles)
            {
                var viewModel = _mapper.Map<AnimalProfileViewModelDetail>(item);
                viewModel.ProfileHistorys = _mapper.Map<List<ProfileHistoryViewModel>>(item.ProfileHistories);
                foreach (var iph in viewModel.ProfileHistorys)
                {
                    iph.CreatedBy = _userRepository.GetByIdAsync(new Guid(iph.CreatedBy)).Result.FullName;
                }
                viewModel.Menus = await GetMenu(item);

                animalProfileViewModel.Add(viewModel);
            }

            return (List<AnimalProfileViewModelDetail>)animalProfileViewModel;
        }

        public async Task<AnimalProfileViewModelDetail?> GetAnimalProfileById(Guid id)
        {
            var animalProfile = await _unitOfWork.AnimalProfileRepository.GetByIdAsync(id);
            if (animalProfile is null || animalProfile.IsDeleted)
            {
                return null;
            }
            var viewModel = _mapper.Map<AnimalProfileViewModelDetail>(animalProfile);
            viewModel.ProfileHistorys = _mapper.Map<List<ProfileHistoryViewModel>>(animalProfile.ProfileHistories);
            foreach (var iph in viewModel.ProfileHistorys)
            {
                iph.CreatedBy = _userRepository.GetByIdAsync(new Guid(iph.CreatedBy)).Result.FullName;
            }
            viewModel.Menus = await GetMenu(animalProfile);
            return viewModel;
        }

        public async Task<List<MenuViewModel>> GetMenu(AnimalProfile animalProfile)
        {
            var date = _currentTime.GetCurrentTime();
            var listMenu = animalProfile.Menus.Where(x => x.StartDate.Value.Day == date.Day &&
                                    x.StartDate.Value.Month == date.Month &&
                                    x.StartDate.Value.Year == date.Year).ToList();

            if (listMenu.Count != 0)
            {
                return _mapper.Map<List<MenuViewModel>>(listMenu);
            }
            else
            {
                listMenu = animalProfile.Menus.Where(x => x.StartDate < _currentTime.GetCurrentTime()).ToList();
                listMenu.Max(x => x.StartDate);
                return _mapper.Map<List<MenuViewModel>>(listMenu);
            }

        }

        public async Task<AnimalProfileViewModel> CreateAnimalProfile(CreateAnimalProfileViewModel model)
        {
            /*var checkName = _unitOfWork.AnimalProfileRepository.GetAllAsync().Result.Where(x => x.Name.ToLower().Equals(name.ToLower()) && x.IsDeleted == false).ToList();
            if (checkName.Count() != 0)
                throw new Exception("Khu vực không được trùng");*/

            /*CreateAnimalProfileViewModel animalProfileView = new()
            {
                Name = name,
                UserId = _claimsService.GetCurrentUserId
            };*/

            var mapper = _mapper.Map<AnimalProfile>(model);
            var count = await _unitOfWork.AnimalProfileRepository.GetAllAsync();
            mapper.ProfileNumber = "AP" + (count.Count + 1);
            mapper.AnimalStatus = AnimalStatus.New;

            await _unitOfWork.AnimalProfileRepository.AddAsync(mapper);

            if (await _unitOfWork.SaveChangeAsync() > 0)
            {
                CreateProfileHistoryViewModel profileHistoryModel = new CreateProfileHistoryViewModel()
                {
                    AnimalProfileId = mapper.Id,
                    CurrentCageId = mapper.CageId,
                    Description = JsonSerializer.Serialize(model),
                    AnimalStatus = mapper.AnimalStatus

                };
                var ph = _mapper.Map<ProfileHistory>(profileHistoryModel);
                ph.CreationDate = _currentTime.GetCurrentTime();
                ph.CreatedBy = _claimsService.GetCurrentUserId;

                await _unitOfWork.ProfileHistoryRepository.AddAsync(ph);

                return await _unitOfWork.SaveChangeAsync() > 0 ? _mapper.Map<AnimalProfileViewModel>(mapper) : throw new Exception("Tạo lịch sử thất bại");
            }
            throw new Exception("Tạo hồ sơ thú thất bại");
        }

        public async Task<AnimalProfileViewModel> UpdateAnimalProfile(UpdateAnimalProfileViewModel model)
        {
            var findAnimalProfile = await _unitOfWork.AnimalProfileRepository.GetByIdAsync(model.Id);

            if (findAnimalProfile is not null && findAnimalProfile.IsDeleted == false)
            {

                var mapper = _mapper.Map(model, findAnimalProfile);
                //mapper.AnimalStatus = AnimalStatus.Updated;
                _unitOfWork.AnimalProfileRepository.Update(mapper);


                if (await _unitOfWork.SaveChangeAsync() > 0)
                {
                    CreateProfileHistoryViewModel profileHistoryModel = new CreateProfileHistoryViewModel()
                    {
                        AnimalProfileId = mapper.Id,
                        CurrentCageId = mapper.CageId,
                        Description = JsonSerializer.Serialize(model, new JsonSerializerOptions
                        {
                            // Thiết lập Encoder để hỗ trợ ký tự Unicode
                            Encoder = System.Text.Encodings.Web.JavaScriptEncoder.UnsafeRelaxedJsonEscaping,
                            DefaultIgnoreCondition = System.Text.Json.Serialization.JsonIgnoreCondition.WhenWritingDefault
                        }),
                        AnimalStatus = mapper.AnimalStatus
                    };
                    var uph = _mapper.Map<ProfileHistory>(profileHistoryModel);
                    uph.CreationDate = _currentTime.GetCurrentTime();
                    uph.CreatedBy = _claimsService.GetCurrentUserId;

                    await _unitOfWork.ProfileHistoryRepository.AddAsync(uph);

                    return await _unitOfWork.SaveChangeAsync() > 0 ? _mapper.Map<AnimalProfileViewModel>(mapper) : throw new Exception("Tạo lịch sử thất bại");
                }
                throw new Exception("Cập nhật hồ sơ thú thất bại");
            }
            throw new Exception("Không tìm thấy hồ sơ thú.");
        }

        public async Task<bool> RemoveAnimalProfile(Guid Id)
        {
            var findAnimalProfile = _animalProfileRepository.getAnimalProfileByIdWithRelation(Id);
            if (findAnimalProfile is not null && findAnimalProfile.IsDeleted == false)
            {
                if (findAnimalProfile.ProfileHistories is null)
                {
                    _unitOfWork.AnimalProfileRepository.Remove(findAnimalProfile);

                    return await _unitOfWork.SaveChangeAsync() > 0 ? true : false;
                }
                else
                {
                    throw new Exception("Xóa hồ sơ thất bại ");
                }
            }

            throw new Exception("Không tìm thấy hồ sơ thú.");

        }

        public async Task<AnimalProfileViewModel> ChangeCage(ChangeCageViewModel model)
        {
            var findAnimalProfile = await _unitOfWork.AnimalProfileRepository.GetByIdAsync(model.AnimalProfileId);

            var findCage = await _unitOfWork.CageRepository.GetByIdAsync(model.NewCageId);

            if (findAnimalProfile is not null && findAnimalProfile.IsDeleted == false)
            {
                if (findCage is not null && findCage.IsDeleted == false)
                {
                    //chỗ này sẽ update cái cage trước, sau đó mới map với update model
                    findAnimalProfile.CageId = model.NewCageId;
                    var mapper = _mapper.Map<UpdateAnimalProfileViewModel>(findAnimalProfile);
                    mapper.AnimalStatus = AnimalStatus.Updated;

                    var profile = await UpdateAnimalProfile(mapper);

                    //return await _unitOfWork.SaveChangeAsync() > 0 ? _mapper.Map<AnimalProfileViewModel>(findAnimalProfile) : throw new Exception("Chuyển chuồng thú thất bại");
                    return profile;
                }
                throw new Exception("Không tìm thất chuồng mới");
            }
            throw new Exception("Không tìm thấy hồ sơ thú");
        }

        public async Task<List<AnimalProfileViewModelDetail>> SearchAnimalProfile(SearchAnimalProfileViewModel model)
        {
            ICollection<AnimalProfileViewModelDetail> animalProfileViewModel = new List<AnimalProfileViewModelDetail>();
            var searchResult = await _unitOfWork.AnimalProfileRepository.SearchAnimalProfile(model);

            if (model.FullUserName == null
                 && model.CageNumber == null
                 && model.PieceId == null
                 && model.AnimalName == null
                 && model.Weight == null
                 && model.BirthDay == null
                 && model.ProfileNumber == null
                 && model.AnimalStatus == null)
                searchResult = await _unitOfWork.AnimalProfileRepository.GetAllAsync();

            foreach (var item in searchResult)
            {
                var viewModel = _mapper.Map<AnimalProfileViewModelDetail>(item);

                viewModel.User = _mapper.Map<UserViewModel>(item.User);
                viewModel.Cage = _mapper.Map<CageViewModel>(item.Cage);
                viewModel.Piece = _mapper.Map<PieceViewModel>(item.Piece);
                viewModel.ProfileHistorys = _mapper.Map<List<ProfileHistoryViewModel>>(item.ProfileHistories);
                foreach (var iph in viewModel.ProfileHistorys)
                {
                    iph.CreatedBy = _userRepository.GetByIdAsync(new Guid(iph.CreatedBy)).Result.FullName;
                }
                animalProfileViewModel.Add(viewModel);
            }

            return (List<AnimalProfileViewModelDetail>)animalProfileViewModel;
        }
    }
}
