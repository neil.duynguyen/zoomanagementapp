﻿using Application.Interfaces;

namespace Application.Services
{
    public class CurrentTime : ICurrentTime
    {
        public DateTime GetCurrentTime()
        {
            TimeZoneInfo vietnamTimeZone = TimeZoneInfo.FindSystemTimeZoneById("SE Asia Standard Time");
            DateTime vietnamTime = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, vietnamTimeZone);
            return vietnamTime;
        }

        public double? Timestamp(DateTime dateTime)
        {
            return dateTime != null ? ((DateTime)dateTime - new DateTime(1970, 1, 1)).TotalSeconds : (double?)null;
        }
    }
}
