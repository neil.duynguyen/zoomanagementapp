﻿using Application.Interfaces;
using Application.ViewModels.TicketViewModels;
using AutoMapper;
using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Services
{
    public class TicketService : ITicketService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly ICurrentTime _currentTime;
        private readonly IMapper _mapper;

        public TicketService(IUnitOfWork unitOfWork, ICurrentTime currentTime, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _currentTime = currentTime;
            _mapper = mapper;
        }

        public async Task<TicketViewModel> CreateTicket(CreateTicketViewModel ticketView)
        {
            var checkName = _unitOfWork.TicketRepository.GetAllAsync().Result.FirstOrDefault(x => x.TicketType.Equals(ticketView.TicketType) && x.IsDeleted == false);

            if (checkName is not null)
                throw new Exception("Loại vé không được trùng.");

            var mapper = _mapper.Map<Ticket>(ticketView);

            await _unitOfWork.TicketRepository.AddAsync(mapper);

            return await _unitOfWork.SaveChangeAsync() > 0 ? _mapper.Map<TicketViewModel>(mapper) : throw new Exception("Tạo vé thất bại");
        }

        public List<TicketViewModel> GetTicket()
        {
            var mapper = _mapper.Map<List<TicketViewModel>>(_unitOfWork.TicketRepository.GetAllAsync().Result.Where(x => x.IsDeleted == false));

            return mapper;
        }

        public async Task<TicketViewModel> GetTicketById(Guid id)
        {
            var ticket = await _unitOfWork.TicketRepository.GetByIdAsync(id);

            if (ticket is null || ticket.IsDeleted)
                throw new Exception("Không tìm thấy vé");

            return _mapper.Map<TicketViewModel>(ticket);
        }

        public async Task<bool> RemoveTicket(Guid Id)
        {
            var findTicket = await _unitOfWork.TicketRepository.GetByIdAsync(Id);

            if (findTicket is not null && findTicket.IsDeleted == false)
            {
                _unitOfWork.TicketRepository.Remove(findTicket);

                return await _unitOfWork.SaveChangeAsync() > 0 ? true : false;
            }

            throw new Exception("Không tìm thấy vé.");
        }

        public async Task<TicketViewModel> UpdateTicket(UpdateTicketModel model)
        {
            var findTicket = await _unitOfWork.TicketRepository.GetByIdAsync(model.Id);

            if (findTicket is not null && findTicket.IsDeleted == false)
            {
                findTicket.Price = model.Price;
                _unitOfWork.TicketRepository.Update(findTicket);

                return await _unitOfWork.SaveChangeAsync() > 0 ? _mapper.Map<TicketViewModel>(findTicket) : throw new Exception("Cập nhật loại vé thất bại");
            }
            throw new Exception("Không tìm thấy loại vé.");
        }
    }
}
