﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.ViewModels.AreaViewModels
{
    public class UpdateAreaViewModel
    {
        public Guid Id { get; set; }
        //public Guid UserId { get; set; }
        public string Name { get; set; }
    }
}
