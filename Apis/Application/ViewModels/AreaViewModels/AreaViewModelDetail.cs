﻿using Application.ViewModels.UserViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.ViewModels.AreaViewModels
{
    public class AreaViewModelDetail
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public UserViewModel User { get; set; }
    }
}
