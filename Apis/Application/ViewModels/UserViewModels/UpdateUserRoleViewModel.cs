﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.ViewModels.UserViewModels
{
    public class UpdateUserRoleViewModel
    {
        public Guid Id { get; set; }
        public Guid RoleId { get; set; }
    }
}
