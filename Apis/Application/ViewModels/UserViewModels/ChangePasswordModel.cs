﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.ViewModels.UserViewModels
{
    public class ChangePasswordModel
    {
        public Guid UserId { get; set; }
        public string OldPassword { get; set;}
        public string NewPassword { get; set;}
        public string ConfirmPassword { get; set;}
        = string.Empty;
        //public string StatusMessage { get; set;}
    }
}
