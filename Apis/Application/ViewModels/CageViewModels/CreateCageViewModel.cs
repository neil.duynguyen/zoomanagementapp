﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.ViewModels.CageViewModels
{
    public class CreateCageViewModel
    {
        public Guid AreaId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public double Square { get; set; }
    }
}
