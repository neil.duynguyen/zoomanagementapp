﻿using Application.ViewModels.AreaViewModels;
using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.ViewModels.CageViewModels
{
    public class CageViewModel
    {
        public Guid Id { get; set; }
        public string CageNumber { get; set; }
        public AreaViewModel Area { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public double Square { get; set; }
    }
}
