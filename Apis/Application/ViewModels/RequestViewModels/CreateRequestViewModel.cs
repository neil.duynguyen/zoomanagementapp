﻿using Domain.Enums;

namespace Application.ViewModels.RequestViewModels
{
    public class CreateRequestViewModel
    {
        //public Guid UserId { get; set; }
        public string RequestType { get; set; }
        public string Description { get; set; }
        public string Detail { get; set; }
        //public RequestStatus RequestStatus { get; set; }
    }
}
