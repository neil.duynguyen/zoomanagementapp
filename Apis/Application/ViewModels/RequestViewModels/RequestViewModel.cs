﻿using Domain.Enums;

namespace Application.ViewModels.RequestViewModels
{
    public class RequestViewModel
    {
        public Guid Id { get; set; }
        public Guid UserId { get; set; }
        public string RequestType { get; set; }
        public string Description { get; set; }
        public string Detail { get; set; }
        public string RequestStatus { get; set; }
        public DateTime CreationDate { get; set; }
    }
}
