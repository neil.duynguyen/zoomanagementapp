﻿using Domain.Entities;
using Domain.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.ViewModels.AnimalProfileViewModels
{
    public class SearchAnimalProfileViewModel
    {

        public string? FullUserName { get; set; }
        //public string? CageNumber { get; set; }
        public string? CageNumber { get; set; }

        public Guid? PieceId { get; set; }
        public string? AnimalName { get; set; }

        public double? Weight { get; set; }
        public DateTime? BirthDay { get; set; }
        public string? ProfileNumber { get; set; }
        public AnimalStatus? AnimalStatus { get; set; }

        /*public virtual User User { get; set; }
        public virtual Cage Cage { get; set; }
        public virtual Piece Piece { get; set; }
        public IList<ProfileHistory> ProfileHistories { get; set; }
        public IList<Menu> Menus { get; set; }*/

    }
}
