﻿using Domain.Enums;
using System.ComponentModel.DataAnnotations;

namespace Application.ViewModels.AnimalProfileViewModels
{
    public class CreateAnimalProfileViewModel
    {
        public Guid UserId { get; set; }
        public Guid CageId { get; set; }
        public Guid PieceId { get; set; }
        public string? Name { get; set; }
        public string? Image { get; set; }
        public string? BirthCertificated { get; set; }
        public double Weight { get; set; }
        public DateTime BirthDay { get; set; }
        //public string ProfileNumber { get; set; }
        //public AnimalStatus AnimalStatus { get; set; }
    }
}
