﻿using Application.ViewModels.CageViewModels;
using Application.ViewModels.MenuViewModels;
using Application.ViewModels.PieceViewModels;
using Application.ViewModels.ProfileHistoryViewModels;
using Application.ViewModels.UserViewModels;
using Domain.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.ViewModels.AnimalProfileViewModels
{
    public class AnimalProfileViewModelDetail
    {     
        public Guid Id { get; set; }
        public string ProfileNumber { get; set; }
        public string? Name { get; set; }
        public double Weight { get; set; }
        public double BirthDay { get; set; }
        public string? Image { get; set; }
        public string? BirthCertificated { get; set; }
        public string AnimalStatus { get; set; }
        public UserViewModel User { get; set; }
        public CageViewModel Cage { get; set; }
        public PieceViewModel Piece { get; set; }
        public List<ProfileHistoryViewModel> ProfileHistorys { get; set; }
        public List<MenuViewModel> Menus { get; set; }
    }
}
