﻿using Domain.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.ViewModels.AnimalProfileViewModels
{
    public class UpdateAnimalProfileViewModel
    {
        public Guid Id { get; set; }
        public Guid UserId { get; set; }
        //public Guid CageId { get; set; }
        public Guid PieceId { get; set; }
        public string? Name { get; set; }
        public string? Image { get; set; }
        public string? BirthCertificated { get; set; }
        public double Weight { get; set; }
        public DateTime BirthDay { get; set; }
        public AnimalStatus AnimalStatus { get; set; }
        public string? Reason { get; set; }
    }
}
