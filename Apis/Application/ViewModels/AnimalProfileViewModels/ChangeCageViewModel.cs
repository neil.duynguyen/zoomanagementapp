﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.ViewModels.AnimalProfileViewModels
{
    public class ChangeCageViewModel
    {
        public Guid AnimalProfileId { get; set; }
        public Guid NewCageId { get; set; }
    }
}
