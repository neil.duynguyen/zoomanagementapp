﻿using Application.ViewModels.CageViewModels;
using Application.ViewModels.PieceViewModels;
using Application.ViewModels.UserViewModels;
using Domain.Entities;
using Domain.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.ViewModels.AnimalProfileViewModels
{
    public class AnimalProfileViewModel
    {
        public Guid UserId { get; set; }
        public Guid CageId { get; set; }
        public Guid PieceId { get; set; }
        public Guid Id { get; set; }
        public string ProfileNumber { get; set; }
        public double BirthDay { get; set; }
        public string? Name { get; set; }
        public double Weight { get; set; }
        public string? Image { get; set; }
        public string? BirthCertificated { get; set; }
        public string AnimalStatus { get; set; }
    }
}
