﻿using Domain.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.ViewModels.MenuViewModels
{
    public class UpdateMenuViewModel
    {
        public Guid Id { get; set; }
        public double ExpectedQuantity { get; set; }
    }
}
