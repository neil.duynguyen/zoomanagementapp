﻿using Domain.Entities;
using Domain.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.ViewModels.MenuViewModels
{
    public class SearchMenuModel
    {
        //public Guid FoodId { get; set; }
        //public Guid AnimalProfileId { get; set; }
        public string? ProfileNumber { get; set; }
        public string? Name { get; set; }
        public Slot? Slot { get; set; }

        public DateTime? StartDate { get; set; }
        //public double? ExpectedQuantity { get; set; }
        /*public virtual Food Food { get; set; }
        public virtual AnimalProfile AnimalProfile { get; set; }
        public IList<Meal> Meals { get; set; }*/
    }
}
