﻿using Domain.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.ViewModels.MenuViewModels
{
    public class MenuViewModel
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string NameFood { get; set; }
        public string Slot { get; set; }

        public double? StartDate { get; set; }
        public double ExpectedQuantity { get; set; }
    }
}
