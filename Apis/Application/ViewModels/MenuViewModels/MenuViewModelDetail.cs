﻿using Domain.Entities;
using Application.ViewModels.FoodViewModels;
using Application.ViewModels.AnimalProfileViewModels;
using Application.ViewModels.MealViewModels;

namespace Application.ViewModels.MenuViewModels
{
    public class MenuViewModelDetail
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        //public string NameFood { get; set; }
        public string Slot { get; set; }

        public double? StartDate { get; set; }
        public double ExpectedQuantity { get; set; }

        public  FoodViewModel Food { get; set; }
        public AnimalProfileViewModel AnimalProfile { get; set; }
        public List<MealViewModel> Meals { get; set; }
    }
}
