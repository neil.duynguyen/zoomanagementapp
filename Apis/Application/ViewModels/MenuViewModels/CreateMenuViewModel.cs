﻿using Domain.Entities;
using Domain.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.ViewModels.MenuViewModels
{
    public class CreateMenuViewModel
    {
        public Guid FoodId { get; set; }
        public Guid AnimalProfileId { get; set; }
        public string Name { get; set; }
        public Slot Slot { get; set; }
        public DateTime? StartDate { get; set; }
        public double ExpectedQuantity { get; set; }
    }
}
