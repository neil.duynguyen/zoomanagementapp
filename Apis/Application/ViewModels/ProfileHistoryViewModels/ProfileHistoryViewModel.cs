﻿using Domain.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.ViewModels.ProfileHistoryViewModels
{
    public class ProfileHistoryViewModel
    {
        public Guid Id { get; set; }
        public Guid AnimalProfileId { get; set; }
        public Guid CurrentCageId { get; set; }
        public string Description { get; set; }
        public string AnimalStatus { get; set; }
        public DateTime CreationDate { get; set; }
        public string CreatedBy { get; set; }
        //public virtual AnimalProfile AnimalProfile { get; set; }
    }
}
