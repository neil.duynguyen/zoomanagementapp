﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.ViewModels.FoodViewModels
{
    public class UpdateFoodViewModel
    {
        public Guid Id { get; set; }
        public double Quantity { get; set; }
        public string? Description { get; set; }
    }
}
