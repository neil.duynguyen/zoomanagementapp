﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.ViewModels.PieceViewModels
{
    public class CreatePieceViewModel
    {
        public string Name { get; set; }
        public double MaxWeight { get; set; }
    }
}
