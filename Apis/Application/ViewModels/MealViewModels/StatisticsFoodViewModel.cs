﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.ViewModels.MealViewModels
{
    public class StatisticsFoodViewModel
    {
        public Guid CageId { get; set; }
        public ICollection<FoodView> Food { get; set; }
    }

    public class FoodView
    {
        public string Name { get; set; }
        public double ActualAmountOfConsumption { get; set; }
        public double ExpectedAmountOfConsumption { get; set; }

    }
}
