﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.ViewModels.MealViewModels
{
    public class MealViewModel
    {
        public double? Date { get; set; }
        public double ActualQuantily { get; set; }
        public Guid MenuId { get; set; }
        public string MenuName { get; set; }
        public string NameAnimal { get; set; }
    }
}
