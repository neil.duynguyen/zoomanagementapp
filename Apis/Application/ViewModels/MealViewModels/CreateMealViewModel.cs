﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.ViewModels.MealViewModels
{
    public class CreateMealViewModel
    {
        public List<CreateMeal> createMeals { get; set; }
    }

    public class CreateMeal
    {
        public Guid MenuId { get; set; }
        public double ActualQuantily { get; set; }

    }
}
