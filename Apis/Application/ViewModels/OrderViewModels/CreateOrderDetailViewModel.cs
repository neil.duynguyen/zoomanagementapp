﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.ViewModels.OrderViewModels
{
    public class CreateOrderDetailViewModel
    {
        public List<OrderDetailViewModel> orderDetailViewModels {  get; set; }
    }
    public class OrderDetailViewModel
    {
        public int Quantity { get; set; }
        public string TicketType { get; set; }
    }
}
