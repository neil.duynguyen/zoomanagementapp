﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.ViewModels.ArticleViewModels
{
    public class UpdateStatusArticleViewModel
    {
        public Guid Id { get; set; }
        public int ArticleStatus {  get; set; }
    }
}
