﻿using Domain.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.ViewModels.ArticleViewModels
{
    public class CreateArticleViewModel
    {
        public string Title { get; set; }
        public string Body { get; set; }
        public string Thumbnail { get; set; }
    }
}
