﻿using Application.ViewModels.UserViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.ViewModels.ArticleViewModels
{
    public class ArticleViewModelDetail
    {
        public Guid Id { get; set; }
        public string Title { get; set; }
        public string Body { get; set; }
        public string Thumbnail { get; set; }
        public string ArticleStatus { get; set; }
        public DateTime CreationDate { get; set; }
        public UserViewModel User { get; set; }
    }
}
