﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.ViewModels.TicketViewModels
{
    public class CreateTicketViewModel
    {
        public double Price { get; set; }
        public string TicketType { get; set; }
    }
}
