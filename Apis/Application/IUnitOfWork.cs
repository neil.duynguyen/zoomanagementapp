﻿using Application.Repositories;

namespace Application
{
    public interface IUnitOfWork
    {
        public IUserRepository UserRepository { get; }
        public IRoleRepository RoleRepository { get; }
        public IPieceRepository PieceRepository { get; }
        public IMenuRepository MenuRepository { get; }
        public IAreaRepository AreaRepository { get; }
        public IFoodRepository FoodRepository { get; }
        public ITicketRepository TicketRepository { get; }
        public ICageRepository CageRepository { get; }
        public IArticleRepository ArticleRepository { get; }
        public IAnimalProfileRepository AnimalProfileRepository { get; }
        public IProfileHistoryRepository ProfileHistoryRepository { get; }
        public IMealRepository MealRepository { get; }
        public IRequestRepository RequestRepository { get; }
        public IOrderRepository OrderRepository { get; }
        public IOrderDetailRepository OrderDetailRepository { get; }


        public Task<int> SaveChangeAsync();
    }
}
