﻿using Application.ViewModels.AnimalProfileViewModels;
using Application.ViewModels.ArticleViewModels;
using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Repositories
{
    public interface IArticleRepository : IGenericRepository<Article>
    {
        public Task<List<Article>> SearchArticle(SearchArticleViewModel model);

    }
}
