﻿using Domain.Entities;

namespace Application.Repositories
{
    public interface ICageRepository : IGenericRepository<Cage>
    {
        public Cage getCageByIdWithRelation(Guid id);

    }
}
