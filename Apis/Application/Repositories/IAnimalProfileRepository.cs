﻿using Application.ViewModels.AnimalProfileViewModels;
using Domain.Entities;

namespace Application.Repositories
{
    public interface IAnimalProfileRepository : IGenericRepository<AnimalProfile>
    {
        public AnimalProfile getAnimalProfileByIdWithRelation(Guid id);
        public Task<List<AnimalProfile>> SearchAnimalProfile(SearchAnimalProfileViewModel model);

    }
}
