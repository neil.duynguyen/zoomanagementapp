﻿using Domain.Entities;

namespace Application.Repositories
{
    public interface IPieceRepository : IGenericRepository<Piece>
    {
        public Piece getPieceByIdWithRelation(Guid id);

    }
}
