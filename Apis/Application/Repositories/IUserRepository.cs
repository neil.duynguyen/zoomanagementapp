﻿using Application.ViewModels.UserViewModels;
using Domain.Entities;

namespace Application.Repositories
{
    public interface IUserRepository : IGenericRepository<User>
    {
        Task<User> GetUserByUserNameAndPasswordHash(string username, string passwordHash);

        Task<bool> CheckUserNameExited(string username);

        Task<User> GetUserByEmail(string email);

        //Task<User> GetUserById(Guid? id);

    }
}
