﻿using Domain.Entities;

namespace Application.Repositories
{
    public interface IAreaRepository : IGenericRepository<Area>
    {
        public Area getAreaByIdWithRelation(Guid id);
    }
}
