﻿using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Repositories
{
    public interface IProfileHistoryRepository : IGenericRepository<ProfileHistory>
    {
        public ProfileHistory getProfileHistoryByIdWithRelation(Guid id);
    }
}
